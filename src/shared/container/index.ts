import { container } from 'tsyringe';

import './providers';

import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { UsersRepository } from '@modules/users/infra/typeorm/repositories/UsersRepository';

import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { PermissionsRepository } from '@modules/acl/infra/typeorm/repositories/PermissionsRepository';

import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { RolesRepository } from '@modules/acl/infra/typeorm/repositories/RolesRepository';

import { IUsersTokensRepository } from '@modules/users/repositories/IUsersTokensRepository';
import { UsersTokensRepository } from '@modules/users/infra/typeorm/repositories/UsersTokensRepository';

import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { WeldersRepository } from '@modules/welders/infra/typeorm/repositories/WeldersRepository';

import { IProcessTypesRepository } from '@modules/welders/repositories/IProcessTypesRepository';
import { ProcessTypesRepository } from '@modules/welders/infra/typeorm/repositories/ProcessTypesRepository';

import { IPNumbersRepository } from '@modules/welders/repositories/IPNumbersRepository';
import { PNumbersRepository } from '@modules/welders/infra/typeorm/repositories/PNumbersRepository';

import { IFNumbersRepository } from '@modules/welders/repositories/IFNumbersRepository';
import { FNumbersRepository } from '@modules/welders/infra/typeorm/repositories/FNumbersRepository';

import { IPositionsRepository } from '@modules/welders/repositories/IPositionsRepository';
import { PositionsRepository } from '@modules/welders/infra/typeorm/repositories/PositionsRepository';

import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';
import { WQCCsRepository } from '@modules/welders/infra/typeorm/repositories/WQCCsRepository';

container.registerSingleton<IUsersRepository>(
  'UsersRepository',
  UsersRepository,
);

container.registerSingleton<IPermissionsRepository>(
  'PermissionsRepository',
  PermissionsRepository,
);

container.registerSingleton<IRolesRepository>(
  'RolesRepository',
  RolesRepository,
);

container.registerSingleton<IUsersTokensRepository>(
  'UsersTokensRepository',
  UsersTokensRepository,
);

container.registerSingleton<IWeldersRepository>(
  'WeldersRepository',
  WeldersRepository,
);

container.registerSingleton<IProcessTypesRepository>(
  'ProcessTypesRepository',
  ProcessTypesRepository,
);

container.registerSingleton<IPNumbersRepository>(
  'PNumbersRepository',
  PNumbersRepository,
);

container.registerSingleton<IFNumbersRepository>(
  'FNumbersRepository',
  FNumbersRepository,
);

container.registerSingleton<IPositionsRepository>(
  'PositionsRepository',
  PositionsRepository,
);

container.registerSingleton<IWQCCsRepository>(
  'WQCCsRepository',
  WQCCsRepository,
);
