import { IStorageProvider } from '../IStorageProvider';

class StorageProviderInMemory implements IStorageProvider {
  private files: string[] = [];

  async save(file: string, _: string): Promise<string> {
    this.files.push(file);

    return file;
  }

  async delete(file: string, _: string): Promise<void> {
    this.files = this.files.filter(f => file === f);
  }
}

export { StorageProviderInMemory };
