import nodemailer, { Transporter } from 'nodemailer';
import { injectable } from 'tsyringe';
import handlebars from 'handlebars';
import fs from 'fs';

import { IMailProvider } from '../IMailProvider';

@injectable()
class EtherealMailProvider implements IMailProvider {
  private client: Transporter;

  constructor() {
    this.client = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.ETHEREAL_USER,
        pass: process.env.ETHEREAL_PASSWORD,
      },
    });
  }

  async sendMail(
    to: string,
    subject: string,
    variables: unknown,
    path: string,
  ): Promise<void> {
    const templateFileContent = fs.readFileSync(path).toString('utf-8');

    const templateParse = handlebars.compile(templateFileContent);

    const templateHTML = templateParse(variables);

    const message = await this.client.sendMail({
      to,
      from: 'Soldagem <programador@programador.me>',
      subject,
      html: templateHTML,
    });

    if (process.env.NODE_ENV !== 'test') {
      console.log('Message sent: %s', message.messageId);
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(message));
    }
  }
}

export { EtherealMailProvider };
