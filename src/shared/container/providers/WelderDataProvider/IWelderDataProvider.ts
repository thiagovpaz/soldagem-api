import XlsxPopulate from 'xlsx-populate';
import {
  IFieldType,
  ISetCellValue,
  IWeldingVariablesType,
  IWQCCItem,
  IWQLItem,
} from './implementations/WelderDataProvider';

interface IWelderDataProvider {
  ws: XlsxPopulate.Sheet;
  totalWQL: number;
  fileToOwner: { id: string; name: string; rqs?: string }[];
  folderName: string;
  WQLItems: IWQLItem[];
  load(file: string): Promise<void>;
  save(name: string): Promise<void>;
  export(welders_ids: string[]): Promise<string>;
  clean(): Promise<void>;
  setFields(fields: IFieldType[]): void;
  findCellById(id: string): IFieldType;
  setCellValue(cellData: ISetCellValue): void;
  buildWQLItems(): void;
  addWV(data: IWeldingVariablesType[]): void;
  addWQCC(data: IWQCCItem): void;
  addHyperlink(url: string): void;
  addImage(user_id: string): Promise<void>;
  addWQLFooter(current_row_number: number): void;
}

export { IWelderDataProvider };
