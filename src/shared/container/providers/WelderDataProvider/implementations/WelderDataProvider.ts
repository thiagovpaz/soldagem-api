import XlsxPopulate from 'xlsx-populate';
import RichText from 'xlsx-populate/lib/RichText';
import QRCode from 'qrcode';
import fs from 'fs';
import libre from 'libreoffice-convert';
import slugify from 'slugify';
import JSZip from 'jszip';
import Excel from 'exceljs';
import { resolve } from 'path';
import { promisify } from 'util';
import { PDFDocument } from 'pdf-lib';
import dayjs from 'dayjs';

import { AppError } from '@shared/errors/AppError';
import { inject, injectable } from 'tsyringe';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { IWelderDataProvider } from '../IWelderDataProvider';

libre.convertAsync = promisify(libre.convert);

export interface IFieldType {
  id: string;
  key: string | string[];
}

export interface ISetCellValue {
  id: string;
  value: string | string[] | number | number[];
  formatter?: string;
}

export interface IWQLItem {
  stamp: string;
  wpq: string;
  name: string;
  process: string;
  pn: string;
  fn: string;
  position: string;
  current_and_polarity: string;
  progression: string;
  thickness: string;
  diameter: string;
  backing: string;
  validity: string;
  wps: string;
}

export interface IWeldingVariablesType {
  field: string;
  title: string | string[];
  params: string | string[] | number | number[];
  range: string | string[] | number | number[];
}

export interface IWQCCItem {
  evidency_of_welding: string;
  date: string;
  validity: string;
  responsible_for_validation: string;
}

@injectable()
class WelderDataProvider implements IWelderDataProvider {
  public workbook: XlsxPopulate.Workbook;

  public ws: XlsxPopulate.Sheet;

  public filename: string;

  public folderName: string;

  public fileToOwner: Partial<Welder[]> = [];

  public link: string;

  public fields: IFieldType[];

  public totalWQL: number;

  WQLItems: IWQLItem[] = [];

  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}

  async load(file: string): Promise<void> {
    try {
      this.workbook = await XlsxPopulate.fromFileAsync(resolve('data', file));
      this.ws = this.workbook.sheet(0);
    } catch (e) {
      throw new AppError('File not found');
    }
  }

  async save(name: string): Promise<void> {
    this.filename = name;

    await fs.promises.mkdir(resolve('tmp', 'certificates', this.folderName), {
      recursive: true,
    });

    await this.workbook.toFileAsync(
      resolve('tmp', 'certificates', this.folderName, `${this.filename}.xlsx`),
    );
  }

  async export(welders_ids: string[]): Promise<string> {
    const exportPDF = true;
    const writePDF = true;
    const zipFiles = true;

    const welder = await this.weldersRepository.findById(welders_ids[0], [
      'user',
      'wqccs',
    ]);

    if (exportPDF) {
      const ext = '.pdf';

      const generatePDF = welders_ids.map(async w => {
        const inputPath = resolve(
          'tmp',
          'certificates',
          this.folderName,
          `${w}.xlsx`,
        );
        const outputPath = resolve(
          'tmp',
          'certificates',
          this.folderName,
          `temp-${w}${ext}`,
        );
        const docxBuf = await fs.promises.readFile(inputPath);
        const pdfBuf = await libre.convertAsync(docxBuf, ext, undefined);
        await fs.promises.writeFile(outputPath, pdfBuf);
      });

      await Promise.all(generatePDF);
    }

    if (writePDF) {
      const readPDFS = welders_ids.map(async w => {
        const existingPdfBytes = await fs.promises.readFile(
          resolve('tmp', 'certificates', this.folderName, `temp-${w}.pdf`),
          'base64',
        );

        const logoImageBytes = await fs.promises.readFile(
          welder?.user?.avatar
            ? resolve('tmp', 'uploads', 'avatars', welder.user.avatar)
            : resolve('data', 'logo.png'),
          'base64',
        );

        const qrImageBytes = await QRCode.toDataURL(this.link, {
          margin: 0,
        });

        const pdfDoc = await PDFDocument.load(existingPdfBytes, {
          updateMetadata: false,
        });

        const pages = pdfDoc.getPages();
        const firstPage = pages[0];
        const secondPage = pages[1];

        const qrImage = await pdfDoc.embedPng(qrImageBytes);
        const qrDims = qrImage.scaleToFit(45, 45);

        const maxW = 106;
        const maxH = 58;
        const logoImage = await pdfDoc.embedPng(logoImageBytes);
        const logoDims = logoImage.scaleToFit(maxW, maxH);

        const { height } = firstPage.getSize();

        firstPage.drawImage(logoImage, {
          x: 70 + (maxW - logoDims.width) / 2,
          y: height - 36.19 - logoDims.height - (maxH - logoDims.height) / 2,
          width: logoDims.width,
          height: logoDims.height,
        });

        firstPage.drawImage(qrImage, {
          x: 230,
          y: qrDims.height + 4.5,
          width: qrDims.width,
          height: qrDims.height,
        });

        secondPage.drawImage(logoImage, {
          x: 70 + (maxW - logoDims.width) / 2,
          y: height - 38 - logoDims.height - (maxH - logoDims.height) / 2,
          width: logoDims.width,
          height: logoDims.height,
        });

        const pdfBytes = await pdfDoc.save();

        await fs.promises.writeFile(
          resolve('tmp', 'certificates', this.folderName, `${w}.pdf`),
          pdfBytes,
        );
      });

      await Promise.all(readPDFS);
    }

    if (zipFiles) {
      if (this.fileToOwner.length > 1) {
        const zip = new JSZip();

        const addFiles = this.fileToOwner.map(async fo => {
          const file = await fs.promises.readFile(
            resolve('tmp', 'certificates', this.folderName, `${fo.id}.pdf`),
          );

          let validity;
          let wqccs_ordered;
          if (fo.wqccs.length > 0) {
            wqccs_ordered = fo.wqccs.sort((a, b) => (a.item > b.item ? -1 : 1));
            validity = wqccs_ordered[0].validity;
            validity = dayjs(validity).utc().local().format('DD-MM-YYYY');
          }

          zip.file(
            `${slugify(
              `RQS-${fo.rqs}.SNT-${fo.stamp}.${
                fo.welding_process.process_name
              }${validity ? `-${validity}` : ''}`,
              {
                lower: false,
              },
            )}.pdf`,
            file,
          );
        });

        await Promise.all(addFiles);

        const data = await zip.generateAsync({ type: 'nodebuffer' });

        await fs.promises.writeFile(
          resolve(
            'tmp',
            'certificates',
            this.folderName,
            `${this.folderName}.zip`,
          ),
          data,
        );

        return resolve(
          'tmp',
          'certificates',
          this.folderName,
          `${this.folderName}.zip`,
        );
      }
    }

    await fs.promises.rename(
      resolve(
        'tmp',
        'certificates',
        this.folderName,
        `${this.fileToOwner[0].id}.pdf`,
      ),
      resolve(
        'tmp',
        'certificates',
        this.folderName,
        `${slugify(this.fileToOwner[0].name, { lower: true })}.pdf`,
      ),
    );

    return resolve(
      'tmp',
      'certificates',
      this.folderName,
      `${slugify(this.fileToOwner[0].name, { lower: true })}.pdf`,
    );
  }

  async clean() {
    // const xlsxTempFile = resolve('tmp', 'certificates', 'wqp-temp.xlsx');
  }

  async rmDir() {
    await fs.promises.rm(resolve('tmp', 'certificates', this.folderName), {
      recursive: true,
    });
  }

  setFields(fields: IFieldType[]) {
    this.fields = fields;
  }

  findCellById(id: string): IFieldType {
    return this.fields.find(p => p.id === id);
  }

  setCellValue({ id, value, formatter = '' }: ISetCellValue): void {
    const cell = this.findCellById(id);

    let updatedCell;

    if (cell.key instanceof Array && value instanceof Array) {
      cell.key.forEach(pos => {
        if (!(value instanceof Array)) {
          updatedCell = this.ws.cell(pos).value(value);
        }
      });
    } else if (cell.key instanceof Array && !(value instanceof Array)) {
      cell.key.forEach(pos => {
        updatedCell = this.ws.cell(pos).value(value);
      });
    } else if (!(cell.key instanceof Array) && value instanceof Array) {
      updatedCell = this.ws.cell(cell.key).value(value[0]);

      const cell_data = this.ws.cell(cell.key);
      const column = cell_data.columnNumber();
      const row = cell_data.rowNumber();

      this.ws
        .row(row + 1)
        .cell(column)
        .value(value[1]);
    } else if (!(cell.key instanceof Array) && !(value instanceof Array)) {
      updatedCell = this.ws.cell(cell.key).value(value);
    }

    const formatters = {
      mm: {
        type: 'numberFormat',
        format: 'General" mm"',
      },
      date: {
        type: 'numberFormat',
        format: 'DD/MM/YYYY',
      },
    };

    if (formatter !== '') {
      updatedCell.style(
        formatters[formatter].type,
        formatters[formatter].format,
      );
    }
  }

  addWQLFooter(current_line_number: number) {
    const spaceRows = 5;

    const fr = current_line_number;

    // (Observations)
    const obs = new RichText();
    obs.add('Observações ', {
      fontFamily: 'Arial',
      fontSize: 12,
      bold: true,
    });

    obs.add(' (Observations)', {
      fontFamily: 'Arial',
      fontSize: 12,
      fontColor: '404040',
      bold: true,
      italic: true,
    });

    const defaultStyleNotes = {
      fontFamily: 'Arial',
      fontSize: 12,
    };

    const notes = new RichText();

    notes.add('Nota 1 =', { ...defaultStyleNotes, bold: true });
    notes.add(' Pnº 1 ao 15F, Pnº 34 e Pnº 41 ao 49', {
      ...defaultStyleNotes,
    });
    notes.add(' (Pnº 1 to 15F, 34 and 41 to 49) /', {
      ...defaultStyleNotes,
      italic: true,
    });
    notes.add(' Nota 2 =', { ...defaultStyleNotes, bold: true });
    notes.add(' Pnº51 ao 53, Pnº61 e Pnº62', {
      ...defaultStyleNotes,
    });
    notes.add(' (Pnº51 to 53, Pnº61 and Pnº62) /', {
      ...defaultStyleNotes,
      italic: true,
    });

    notes.add(' Nota 3 =', { ...defaultStyleNotes, bold: true });
    notes.add(' Grupo I, II, III e IV', {
      ...defaultStyleNotes,
    });
    notes.add(' (Group I, II, III and IV)', {
      ...defaultStyleNotes,
      italic: true,
    });

    this.ws.row(fr + 1).height(25);
    this.ws.row(fr + 2).height(25);
    this.ws
      .range(`A${fr + 1}:N${fr + 1}`)
      .value(obs)
      .merged(true)
      .style({
        border: {
          top: true,
          bottom: false,
          left: true,
          right: true,
        },
      });

    this.ws
      .range(`A${fr + 2}:N${fr + 2}`)
      .value(notes)
      .merged(true)
      .style({
        border: { top: false, bottom: true, left: true, right: true },
      });

    const defaultLabelsStyles = {
      border: true,
      verticalAlignment: 'center',
      horizontalAlignment: 'center',
    };

    // Welder Inspector
    const wi = new RichText();
    wi.add('INSPETOR DE SOLDAGEM\n', { fontSize: 12, bold: true });
    wi.add('Welder Inspector', {
      fontSize: 12,
      italic: true,
      fontColor: '404040',
    });

    const stamp = new RichText();
    stamp.add('Rodrigo Carneiro da Silva\n', {
      fontFamily: 'Arial',
      fontSize: 16,
    });
    stamp.add('SNQC IS 11207 N2\n', {
      bold: true,
      fontFamily: 'Book Antiqua',
      fontSize: 14,
    });
    stamp.add('Cel. (41) 9 9214-7678', {
      fontFamily: 'Calibri',
      fontSize: 14,
    });
    this.ws.row(fr + 3).height(15);
    this.ws.row(fr + 4).height(35);
    this.ws
      .range(`A${fr + 4}:C${fr + 4}`)
      .value(wi)
      .merged(true)
      .style({ ...defaultLabelsStyles, fill: 'd8d8d9' });
    this.ws
      .range(`A${fr + 5}:C${fr + 5 + spaceRows}`)
      .value(stamp)
      .merged(true)
      .style(defaultLabelsStyles);

    // Quality
    const qc = new RichText();
    qc.add('CORDENAÇÃO DA QUALIDADE\n', { fontSize: 12, bold: true });
    qc.add('Quality Coordination', {
      fontSize: 12,
      italic: true,
      fontColor: '404040',
    });
    this.ws
      .range(`D${fr + 4}:I${fr + 4}`)
      .value(qc)
      .merged(true)
      .style({ ...defaultLabelsStyles, fill: 'd8d8d9' });
    this.ws
      .range(`D${fr + 5}:I${fr + 5 + spaceRows}`)
      .value('')
      .merged(true)
      .style({
        border: true,
        verticalAlignment: 'center',
        horizontalAlignment: 'center',
      });
    // Customer Approval
    const ca = new RichText();
    ca.add('APROVAÇÃO CLIENTE\n', { fontSize: 12, bold: true });
    ca.add('Customer Approval', {
      fontSize: 12,
      italic: true,
      fontColor: '404040',
    });
    this.ws
      .range(`J${fr + 4}:N${fr + 4}`)
      .value(ca)
      .merged(true)
      .style({ ...defaultLabelsStyles, fill: 'd8d8d9' });
    this.ws
      .range(`J${fr + 5}:N${fr + 5 + spaceRows}`)
      .value('')
      .merged(true)
      .style({ ...defaultLabelsStyles });
  }

  buildWQLItems(): void {
    const WQL_START = 7;
    let last_row = WQL_START;

    const defaultBorderStyle = { border: { top: true, bottom: true } };

    this.WQLItems.forEach(
      ({
        stamp,
        wpq,
        name,
        process,
        pn,
        fn,
        position,
        current_and_polarity,
        progression,
        thickness,
        diameter,
        backing,
        validity,
        wps,
      }: IWQLItem) => {
        const current_row = this.ws.row(last_row + 1);

        current_row.style({
          verticalAlignment: 'center',
          horizontalAlignment: 'center',
          fontFamily: 'Arial',
          bold: false,
          fontSize: 12,
        });

        current_row
          .cell(1)
          .value(parseInt(stamp, 10))
          .style({ ...defaultBorderStyle, numberFormat: '0' });
        current_row.cell(2).value(wpq).style(defaultBorderStyle);
        current_row.cell(3).value(name).style(defaultBorderStyle);
        current_row.cell(4).value(process).style(defaultBorderStyle);
        current_row.cell(5).value(pn).style(defaultBorderStyle);
        current_row.cell(6).value(fn).style(defaultBorderStyle);
        current_row.cell(7).value(position).style(defaultBorderStyle);
        current_row
          .cell(8)
          .value(current_and_polarity)
          .style(defaultBorderStyle);
        current_row.cell(9).value(progression).style(defaultBorderStyle);
        current_row.cell(10).value(thickness).style(defaultBorderStyle);
        current_row.cell(11).value(diameter).style(defaultBorderStyle);
        current_row.cell(12).value(backing).style(defaultBorderStyle);
        current_row.cell(13).value(validity).style(defaultBorderStyle);
        current_row.cell(14).value(wps).style(defaultBorderStyle);
        last_row += 1;
      },
    );

    function filler(fillCount: number, ws) {
      for (let fill = 1; fill <= fillCount; fill += 1) {
        const current_row = ws.row(last_row + 1);
        current_row.cell(1).value('');
        last_row += 1;
      }
    }

    if (last_row - WQL_START + 11 <= 35) {
      filler(35 - last_row - WQL_START, this.ws);
    } else {
      filler(35 - ((last_row - WQL_START) % 35), this.ws);
    }

    this.addWQLFooter(last_row + 1);
  }

  addWV(data: IWeldingVariablesType[]) {
    const WV_START = 13;
    let current_line_number = WV_START;

    data.forEach(v => {
      const current_row = this.ws.row(current_line_number);
      const next_row = this.ws.row(current_line_number + 1);

      const defaultStyle = {
        verticalAlignment: 'center',
        horizontalAlignment: 'center',
        fontFamily: 'Arial',
      };

      if (v.title instanceof Array) {
        current_row.cell(1).value(v.title[0]);
        next_row.cell(1).value(v.title[1]);
      }

      if (v.params instanceof Array) {
        this.ws.cell(`V${current_line_number}`).value(v.params[0]);
        this.ws.cell(`V${current_line_number + 1}`).value(v.params[1]);
      } else {
        this.ws
          .range(`V${current_line_number}:AP${current_line_number}`)
          .merged(false);
        this.ws
          .range(`V${current_line_number + 1}:AP${current_line_number + 1}`)
          .merged(false);
        this.ws
          .range(`V${current_line_number}:AP${current_line_number + 1}`)
          .merged(true);
        this.ws
          .cell(`V${current_line_number}`)
          .value(v.params)
          .style(defaultStyle);
      }

      if (v.range instanceof Array) {
        this.ws.cell(`AQ${current_line_number}`).value(v.range[0]);
        this.ws.cell(`AQ${current_line_number + 1}`).value(v.range[1]);
      } else {
        this.ws
          .range(`AQ${current_line_number}:BK${current_line_number}`)
          .merged(false);
        this.ws
          .range(`AQ${current_line_number + 1}:BK${current_line_number + 1}`)
          .merged(false);
        this.ws
          .range(`AQ${current_line_number}:BK${current_line_number + 1}`)
          .merged(true);
        this.ws
          .cell(`AQ${current_line_number}`)
          .value(v.range)
          .style(defaultStyle);
      }

      current_line_number += 2;
    });
  }

  addWQCC({
    evidency_of_welding,
    date,
    validity,
    responsible_for_validation,
  }: IWQCCItem): void {
    const WQCC_START = 76;
    let current_line_number = WQCC_START;
    let lines = 0;

    do {
      current_line_number += 1;
      lines += 1;
    } while (this.ws.row(current_line_number).cell(1).value());

    const current_row = this.ws.row(current_line_number);

    current_row.cell(1).value(lines);
    this.ws.cell(`D${current_line_number}`).value(evidency_of_welding);
    this.ws.cell(`AA${current_line_number}`).value(date);
    this.ws.cell(`AI${current_line_number}`).value(validity);
    this.ws.cell(`AQ${current_line_number}`).value(responsible_for_validation);
  }

  addHyperlink(url: string) {
    this.link = url;

    const rightSpace = '          ';

    this.ws
      .cell('A62')
      .value(`${url}${rightSpace}`)
      .style({
        fontColor: '000000',
        fontFamily: 'Calibri',
        fontSize: 8,
        underline: false,
        bold: true,
        verticalAlignment: 'bottom',
        horizontalAlignment: 'right',
      })
      .hyperlink({
        hyperlink: url,
      });
  }

  async addImage(image?: string) {
    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile(resolve('data', `wql-22.03.21-r2.xlsx`));
    const worksheet = workbook.getWorksheet('WQL');

    const logo = workbook.addImage({
      filename: image
        ? resolve('tmp', 'uploads', 'avatars', image)
        : resolve('data', 'logo.png'),
      extension: 'png',
    });

    // width: 620, height: 130
    worksheet.addImage(logo, {
      tl: { col: 1.95, row: 0 },
      ext: { width: 285, height: 130 },
    });

    await workbook.xlsx.writeFile(
      resolve('data', `wql-22.03.21-r2-with-logo.xlsx`),
    );
  }
}

export { WelderDataProvider };
