import XlsxPopulate from 'xlsx-populate';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import {
  IFieldType,
  ISetCellValue,
  IWeldingVariablesType,
  IWQCCItem,
  IWQLItem,
} from '../implementations/WelderDataProvider';
import { IWelderDataProvider } from '../IWelderDataProvider';

class WelderDataProviderInMemory implements IWelderDataProvider {
  public workbook: IWQLItem[] = [];

  public fake_ws: ISetCellValue[] = [];

  public ws: XlsxPopulate.Sheet;

  public filename: string;

  public folderName: string;

  public fileToOwner: Partial<Welder[]>;

  public url: string;

  public fields: IFieldType[] = [];

  public wvs = [];

  public wqcc: IWQCCItem[] = [];

  public totalWQL: number;

  WQLItems: IWQLItem[] = [];

  async load(file: string): Promise<void> {
    this.filename = file;
  }

  async save(): Promise<void> {
    // TODO: fake-save
  }

  async export(_welders_ids: string[]): Promise<string> {
    return 'fake-filename.zip';
  }

  async clean(): Promise<void> {
    // TODO
  }

  setFields(fields: IFieldType[]): void {
    this.fields = fields;
  }

  findCellById(id: string): IFieldType {
    return this.fields[id];
  }

  setCellValue(cellData: ISetCellValue): void {
    this.fake_ws.push(cellData);
  }

  buildWQLItems(): void {
    //
  }

  addWV(data: IWeldingVariablesType[]): void {
    this.wvs.push(data);
  }

  addWQCC(data: IWQCCItem): void {
    this.wqcc.push(data);
  }

  addHyperlink(url: string): void {
    this.url = url;
  }

  async addImage(user_id: string): Promise<void> {
    //
  }

  addWQLFooter() {
    // TODO: fake-footer
  }
}

export { WelderDataProviderInMemory };
