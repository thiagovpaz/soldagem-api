import { container } from 'tsyringe';

import { WelderDataProvider } from './implementations/WelderDataProvider';
import { IWelderDataProvider } from './IWelderDataProvider';

container.registerSingleton<IWelderDataProvider>(
  'WelderDataProvider',
  WelderDataProvider,
);
