import { Router } from 'express';

import { usersRouter } from '@modules/users/infra/http/routes/users.routes';
import { authRouter } from '@modules/users/infra/http/routes/auth.routes';
import { permissionsRouter } from '@modules/acl/infra/http/routes/permissions.routes';
import { rolesRouter } from '@modules/acl/infra/http/routes/roles.routes';
import { aclRouter } from '@modules/acl/infra/http/routes/acl.routes';
import { passwordsRouter } from '@modules/users/infra/http/routes/passwords.routes';
import { profileRouter } from '@modules/users/infra/http/routes/profile.routes';
import { weldersRouter } from '@modules/welders/infra/http/routes/welders.routes';
import { contactRouter } from '@modules/contact/infra/http/routes/contact.routes';

const mainRouter = Router();

mainRouter.use('/users', usersRouter);
mainRouter.use('/auth', authRouter);
mainRouter.use('/permissions', permissionsRouter);
mainRouter.use('/roles', rolesRouter);
mainRouter.use('/acl', aclRouter);
mainRouter.use('/passwords', passwordsRouter);
mainRouter.use('/me', profileRouter);
mainRouter.use('/welders', weldersRouter);
mainRouter.use('/contact', contactRouter);

export { mainRouter };
