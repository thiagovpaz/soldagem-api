import 'dotenv/config';
import 'reflect-metadata';
import express, { Request, Response, NextFunction } from 'express';
import 'express-async-errors';
import cors from 'cors';

import '@shared/container';
import upload from '@config/upload';
import createConnection from '@shared/infra/typeorm';

import { AppError } from '@shared/errors/AppError';
import { mainRouter } from './routes';

createConnection();
const app = express();

app.use(express.json());
app.use(cors());

app.use('/v1', mainRouter);

app.use('/avatars', express.static(`${upload.tmpFolder}/avatars`));

app.use((err: Error, request: Request, response: Response, _: NextFunction) => {
  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      message: err.message,
    });
  }

  console.error(err);

  return response.status(500).json({
    status: 'error',
    message: `Internal server error - ${err.message}`,
  });
});

export { app };
