import {
  Connection,
  createConnection,
  getConnectionOptions,
  ConnectionOptions,
} from 'typeorm';

export default async (): Promise<Connection> => {
  let configOptions: ConnectionOptions;

  if (process.env.NODE_ENV === 'test') {
    configOptions = await getConnectionOptions('test');
  } else {
    configOptions = await getConnectionOptions();
  }

  return createConnection({ ...configOptions, name: 'default' });
};
