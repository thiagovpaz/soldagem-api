import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { PNumber } from '@modules/welders/infra/typeorm/entities/PNumber';

define(PNumber, (faker: typeof Faker) => {
  const p_number = new PNumber();

  p_number.pn = faker.name.findName();
  p_number.qualified_range = faker.name.findName();

  return p_number;
});
