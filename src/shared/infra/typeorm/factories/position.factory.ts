import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { Position } from '@modules/welders/infra/typeorm/entities/Position';

define(Position, (faker: typeof Faker) => {
  const position = new Position();

  position.test_position = faker.name.findName();
  position.qualified_position = faker.name.findName();

  return position;
});
