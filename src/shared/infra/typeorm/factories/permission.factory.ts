import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';

define(Permission, (faker: typeof Faker) => {
  const permission = new Permission();

  permission.name = faker.name.findName();
  permission.description = faker.name.findName();

  return permission;
});
