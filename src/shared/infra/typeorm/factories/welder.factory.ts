import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';

define(Welder, (faker: typeof Faker) => {
  const welder = new Welder();

  welder.name = faker.name.findName();

  return welder;
});
