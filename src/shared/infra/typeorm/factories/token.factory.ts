import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';
import { v4 as uuidv4 } from 'uuid';

import { UserToken } from '@modules/users/infra/typeorm/entities/UserToken';

define(UserToken, (faker: typeof Faker) => {
  const user_token = new UserToken();

  user_token.expires_date = faker.date.future();
  user_token.refresh_token = uuidv4();
  user_token.user_id = uuidv4();

  return user_token;
});
