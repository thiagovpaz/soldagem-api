import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';
import { hashSync } from 'bcrypt';

import { User } from '@modules/users/infra/typeorm/entities/User';

define(User, (faker: typeof Faker) => {
  const user = new User();

  user.name = faker.name.findName();
  user.email = faker.internet.email().toLowerCase();
  user.password = hashSync('secret', 1);
  user.avatar = faker.image.avatar();
  user.status = false;

  return user;
});
