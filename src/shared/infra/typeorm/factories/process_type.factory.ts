import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { ProcessType } from '@modules/welders/infra/typeorm/entities/ProcessType';

define(ProcessType, (faker: typeof Faker) => {
  const process_type = new ProcessType();

  process_type.process_name = faker.name.findName();
  process_type.process_type = faker.name.findName();

  return process_type;
});
