import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';

define(WQCC, (faker: typeof Faker) => {
  const wqcc = new WQCC();

  wqcc.evidency_welding = faker.company.bsNoun();
  wqcc.date = faker.date.future();
  wqcc.validity = faker.date.future();
  wqcc.validation_responsible = faker.name.findName();

  return wqcc;
});
