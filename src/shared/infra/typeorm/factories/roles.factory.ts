import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { Role } from '@modules/acl/infra/typeorm/entities/Role';

define(Role, (faker: typeof Faker) => {
  const role = new Role();

  role.name = faker.name.findName();
  role.description = faker.name.findName();

  return role;
});
