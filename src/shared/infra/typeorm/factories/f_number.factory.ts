import { faker as Faker } from '@faker-js/faker';
import { define } from 'typeorm-seeding';

import { FNumber } from '@modules/welders/infra/typeorm/entities/FNumber';

define(FNumber, (faker: typeof Faker) => {
  const f_number = new FNumber();

  f_number.fn = faker.name.findName();
  f_number.qualified_range = faker.name.findName();

  return f_number;
});
