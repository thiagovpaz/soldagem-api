import { Factory, Seeder } from 'typeorm-seeding';
import { v4 } from 'uuid';

import { PNumber } from '@modules/welders/infra/typeorm/entities/PNumber';

export class CreatePNumbers implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const DEFAULT_P_NUMBERS = [
      {
        pn: '1',
        qualified_range: '1 ao 15F, 34 e 41 ao 49 | 1 to 15F, 34 and 41 to 49',
      },
      {
        id: 'c14ec319-bad3-424b-b29d-973c39bd9e9e',
        pn: '51',
        qualified_range: '51 ao 53, 61 e 62 | 51 to 53, 61 and 62',
      },
    ];

    const promiseSave = DEFAULT_P_NUMBERS.map(
      async ({ id, pn, qualified_range }) => {
        await factory(PNumber)().create({
          id: id || v4(),
          pn,
          qualified_range,
        });
      },
    );

    await Promise.all(promiseSave);
  }
}
