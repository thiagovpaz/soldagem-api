import { Factory, Seeder } from 'typeorm-seeding';

import { User } from '@modules/users/infra/typeorm/entities/User';

export class CreateUsers implements Seeder {
  public async run(factory: Factory): Promise<void> {
    await factory(User)().create({
      id: '8c906152-4a46-46e0-b852-163e24928293',
      name: 'User',
      email: 'user@user.com',
      password: 'secret',
      avatar: null,
    });

    await factory(User)().createMany(5);
  }
}
