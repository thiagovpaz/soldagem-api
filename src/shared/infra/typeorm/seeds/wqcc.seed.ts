import { Factory, Seeder } from 'typeorm-seeding';

import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';

export class CreateWQCCs implements Seeder {
  public async run(factory: Factory): Promise<void> {
    await factory(WQCC)().create({
      id: '15d3257f-fcc0-42b6-a207-662175912c82',
      welder_id: '4d370e58-dad4-405b-8c95-7e5ce9c4c9bb',
      item: 1,
    });
  }
}
