import { Factory, Seeder } from 'typeorm-seeding';

import { Role } from '@modules/acl/infra/typeorm/entities/Role';
import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';

export class CreateRolesAndPermissions implements Seeder {
  public async run(factory: Factory): Promise<void> {
    await factory(Permission)().create({
      name: 'welder/*',
      description: 'A - Controle de Soldadores',
    });

    await factory(Permission)().create({
      name: 'welder/qualify',
      description: 'B - Qualificação de soldadores',
    });

    await factory(Role)().create({
      name: 'GM',
      description: 'Gestor Master',
    });

    await factory(Role)().create({
      name: 'GO',
      description: 'Gestor Obra',
    });

    await factory(Role)().create({
      name: 'INSPECTOR',
      description: 'Inspetor',
    });

    await factory(Role)().create({
      name: 'SUBCONTRACTOR',
      description: 'Subcontratada',
    });

    await factory(Role)().create({
      name: 'CLIENT',
      description: 'Cliente final',
    });
  }
}
