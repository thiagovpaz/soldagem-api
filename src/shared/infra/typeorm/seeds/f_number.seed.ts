import { Factory, Seeder } from 'typeorm-seeding';
import { v4 } from 'uuid';

import { FNumber } from '@modules/welders/infra/typeorm/entities/FNumber';

export class CreateFNumbers implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const DEFAULT_F_NUMBERS = [
      {
        fn: '4',
        qualified_range: '1 e 4 | 1 and 4',
      },
      {
        fn: '5',
        qualified_range: '5',
      },
      {
        fn: '6',
        qualified_range: '6',
      },
      {
        fn: '51',
        qualified_range: '51',
        id: '3ba83044-e67d-4171-9855-4f7cea47611f',
      },
    ];

    const promiseSave = DEFAULT_F_NUMBERS.map(
      async ({ id, fn, qualified_range }) => {
        await factory(FNumber)().create({
          id: id || v4(),
          fn,
          qualified_range,
        });
      },
    );

    await Promise.all(promiseSave);
  }
}
