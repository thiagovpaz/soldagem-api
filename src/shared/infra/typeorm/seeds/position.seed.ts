import { Factory, Seeder } from 'typeorm-seeding';
import { v4 } from 'uuid';

import { Position } from '@modules/welders/infra/typeorm/entities/Position';

export class CreatePositions implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const DEFAULT_POSITIONS = [
      {
        id: 'dac45f63-84b7-42c3-8205-3ecca8e6e9db',
        test_position: '1G',
        qualified_position: 'P',
      },
      {
        test_position: '2G',
        qualified_position: 'F e H / F and H ',
      },
      {
        test_position: '6G',
        qualified_position: 'Todas | All',
      },
    ];

    const promiseSave = DEFAULT_POSITIONS.map(
      async ({ id, test_position, qualified_position }) => {
        await factory(Position)().create({
          id: id || v4(),
          test_position,
          qualified_position,
        });
      },
    );

    await Promise.all(promiseSave);
  }
}
