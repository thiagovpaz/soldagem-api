import { Factory, Seeder } from 'typeorm-seeding';

import { User } from '@modules/users/infra/typeorm/entities/User';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';
import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';

export class CreateAdmin implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const permission = await factory(Permission)().create({
      name: '*',
      description: '** Permissão de Administrador **',
    });

    const role = await factory(Role)().create({
      name: 'ADMIN',
      description: 'Administrador',
      permissions: [permission],
    });

    await factory(User)().create({
      id: '659562a4-3a48-4c9e-862c-35b962b6f039',
      name: 'admin',
      email: 'admin@admin.com',
      permissions: [permission],
      roles: [role],
      avatar: null,
    });
  }
}
