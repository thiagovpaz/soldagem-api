import { Factory, Seeder } from 'typeorm-seeding';

import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';

export class CreateWelder implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const base_welder = {
      user_id: '659562a4-3a48-4c9e-862c-35b962b6f039',
      name: 'OSMAR JOSE DA SILVA',
      cpf: '022.931.669-70',
      stamp: 1,
      rqs: '1-22',
      wps: 'UTM-EPS-01',
      code: 'ASME IX',
      code_edition: '2021',
      base_metal: 'A-106 Gr B',
      thickness: 6.35,
      observation: 'Nenhuma',
      result_ve: false,
      executant: 'Maicon R. Scheid',
      report: '2022/US181',
      result_mvt: false,
      process_type: 1,
      test_type: 0,
      backing: false,
      thickness_gw: 6.35,
      thickness_fw: 0,
      diameter_groove_weld: 60.3,
      diameter_filled_weld: 0,
      specification: 'A 5.18',
      classification: 'ER70S-3',
      progression: 1,
      shielding_gas: 'Argônio | Argon',
      backing_gas: false,
      eletrical_ckp: 0,
      transfer_mode: 0,
      date: new Date(),
    };

    await factory(Welder)().create({
      ...base_welder,
      id: '4d370e58-dad4-405b-8c95-7e5ce9c4c9bb',
      welding_process_id: '78c6f757-add6-47f0-a964-3a9634dc6929',
      p_number_id: 'c14ec319-bad3-424b-b29d-973c39bd9e9e',
      f_number_id: '3ba83044-e67d-4171-9855-4f7cea47611f',
      position_id: 'dac45f63-84b7-42c3-8205-3ecca8e6e9db',
      stamp: 2,
    });

    await factory(Welder)().create({
      ...base_welder,
      id: 'ecc1d4e9-0ab5-42e1-b0eb-fc4b4a3c8d6a',
      welding_process_id: '28cd1521-fbbc-4ce2-9786-02bff8b51b34',
      p_number_id: 'c14ec319-bad3-424b-b29d-973c39bd9e9e',
      f_number_id: '3ba83044-e67d-4171-9855-4f7cea47611f',
      position_id: 'dac45f63-84b7-42c3-8205-3ecca8e6e9db',
      stamp: 3,
    });

    await factory(Welder)().create({
      ...base_welder,
      id: '19ba407f-6ec1-440a-8c6b-3ee5af089251',
      stamp: 4,
    });
  }
}
