import { Factory, Seeder } from 'typeorm-seeding';
import { v4 } from 'uuid';

import { ProcessType } from '@modules/welders/infra/typeorm/entities/ProcessType';

export class CreateProcessTypes implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const DEFAULT_PROCESS_TYPES = [
      {
        process_name: 'FCAW',
        process_type: 'Semi-automático | Semi automatic',
      },
      {
        id: '78c6f757-add6-47f0-a964-3a9634dc6929',
        process_name: 'GMAW',
        process_type: 'Semi-automático | Semi automatic',
      },
      {
        id: '1d58dbf7-f5dd-446b-82e2-55383a5074a6',
        process_name: 'GTAW',
        process_type: 'Manual / Semi-automático | Manual / Semi automatic',
      },
      {
        process_name: 'SAW',
        process_type: 'Semi-automático | Semi automatic',
      },
      {
        id: '28cd1521-fbbc-4ce2-9786-02bff8b51b34',
        process_name: 'SMAW',
        process_type: 'Manual | Manual',
      },
      {
        process_name: 'PAW',
        process_type: 'Manual / Semi-automático | Manual / Semi automatic',
      },
    ];

    const promiseSave = DEFAULT_PROCESS_TYPES.map(
      async ({ id, process_name, process_type }) => {
        await factory(ProcessType)().create({
          id: id || v4(),
          process_name,
          process_type,
        });
      },
    );

    await Promise.all(promiseSave);
  }
}
