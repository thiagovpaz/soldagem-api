import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateWQCCs1649217863256 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'wqccs',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
          },
          {
            name: 'welder_id',
            type: 'uuid',
          },
          {
            name: 'item',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'evidency_welding',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'date',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'validity',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'validation_responsible',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
        foreignKeys: [
          {
            columnNames: ['welder_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'welders',
            name: 'fk_welders_wqccs_',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('wqccs');
  }
}
