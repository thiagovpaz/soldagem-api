import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateWelders1649044114873 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'welders',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
          },
          {
            name: 'user_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'welding_process_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'p_number_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'position_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'f_number_id',
            type: 'uuid',
            isNullable: true,
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'cpf',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'stamp',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'rqs',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'wps',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'code',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'code_edition',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'base_metal',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'thickness',
            type: 'decimal',
            isNullable: true,
          },
          {
            name: 'observation',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'result_ve',
            type: 'boolean',
            isNullable: true,
          },
          {
            name: 'test_type',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'executant',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'report',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'result_mvt',
            type: 'boolean',
            isNullable: true,
          },
          {
            name: 'process_type',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'backing',
            type: 'boolean',
            isNullable: true,
          },
          {
            name: 'thickness_gw',
            type: 'decimal',
            isNullable: true,
          },
          {
            name: 'thickness_fw',
            type: 'decimal',
            isNullable: true,
          },
          {
            name: 'diameter_groove_weld',
            type: 'decimal',
            isNullable: true,
          },
          {
            name: 'diameter_filled_weld',
            type: 'decimal',
            isNullable: true,
          },
          {
            name: 'specification',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'classification',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'progression',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'shielding_gas',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'backing_gas',
            type: 'boolean',
            isNullable: true,
          },
          {
            name: 'eletrical_ckp',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'transfer_mode',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'date',
            type: 'timestamp',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
        foreignKeys: [
          {
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            name: 'fk_users_welders_',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          {
            columnNames: ['welding_process_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'process_types',
            name: 'fk_process_types_welders',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          {
            columnNames: ['p_number_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'p_numbers',
            name: 'fk_p_numbers_welders',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          {
            columnNames: ['f_number_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'f_numbers',
            name: 'fk_f_numbers_welders',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          {
            columnNames: ['position_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'positions',
            name: 'fk_positions_welders',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('welders');
  }
}
