import crypto from 'crypto';
import multer from 'multer';
import mime from 'mime';
import { resolve } from 'path';

import { AppError } from '@shared/errors/AppError';

const tmpFolder = resolve(__dirname, '..', '..', 'tmp', 'uploads');

const allowedTypes = ['image/png', 'image/jpeg', 'image/gif'];

interface IUploadConfig {
  driver: 's3' | 'local';

  tmpFolder: string;

  multer: {
    storage: multer.StorageEngine;
  };

  config: {
    disk: object;
    aws: {
      bucket: string;
    };
  };
}

export default {
  driver: process.env.STORAGE_DRIVER,
  tmpFolder,
  multer: {
    storage: multer.diskStorage({
      destination: tmpFolder,
      filename: (request, file, callback) => {
        const fileHash = crypto.randomBytes(16).toString('hex');
        const fileName = `${fileHash}.${mime.getExtension(file.mimetype)}`;

        return callback(null, fileName);
      },
    }),
    fileFilter: (request, file, cb) => {
      if (allowedTypes.includes(file.mimetype)) {
        return cb(null, true);
      }
      cb(null, false);
      return cb(
        new AppError(
          `Only ${allowedTypes
            .map(a => mime.getExtension(a))
            .join(', ')} formats are allowed`,
        ),
      );
    },
  },
  config: {
    disk: {},
    aws: {
      bucket: process.env.AWS_BUCKET,
    },
  },
} as IUploadConfig;
