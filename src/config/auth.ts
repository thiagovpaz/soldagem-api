export default {
  secret_token: process.env.APP_SECRET_TOKEN,
  expires_in_token: '15d',
};
