import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './BaseEntity';
import { Welder } from './Welder';

@Entity('wqccs')
class WQCC extends BaseEntity {
  @Column()
  welder_id: string;

  @Column()
  item: number;

  @Column()
  evidency_welding: string;

  @Column()
  date: Date;

  @Column()
  validity: Date;

  @Column()
  validation_responsible: string;

  @ManyToOne(() => Welder, welder => welder.wqccs)
  @JoinColumn({ name: 'welder_id', referencedColumnName: 'id' })
  welder: Welder;
}

export { WQCC };
