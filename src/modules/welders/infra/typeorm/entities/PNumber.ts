import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('p_numbers')
class PNumber extends BaseEntity {
  @Column()
  pn: string;

  @Column()
  qualified_range: string;
}

export { PNumber };
