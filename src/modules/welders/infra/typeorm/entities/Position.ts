import { Column, Entity } from 'typeorm';

import { BaseEntity } from './BaseEntity';

@Entity('positions')
class Position extends BaseEntity {
  @Column()
  test_position: string;

  @Column()
  qualified_position: string;
}

export { Position };
