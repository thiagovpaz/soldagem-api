import { Column, Entity } from 'typeorm';

import { BaseEntity } from './BaseEntity';

@Entity('process_types')
class ProcessType extends BaseEntity {
  @Column()
  process_type: string;

  @Column()
  process_name: string;
}

export { ProcessType };
