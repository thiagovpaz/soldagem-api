import { Entity, Column } from 'typeorm';
import { BaseEntity } from './BaseEntity';

@Entity('f_numbers')
class FNumber extends BaseEntity {
  @Column()
  fn: string;

  @Column()
  qualified_range: string;
}

export { FNumber };
