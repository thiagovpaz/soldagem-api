import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { User } from '@modules/users/infra/typeorm/entities/User';
import { Position } from './Position';
import { ProcessType } from './ProcessType';
import { BaseEntity } from './BaseEntity';
import { WQCC } from './WQCC';
import { PNumber } from './PNumber';
import { FNumber } from './FNumber';

@Entity('welders')
class Welder extends BaseEntity {
  @Column()
  user_id: string;

  @Column()
  welding_process_id: string;

  @Column()
  p_number_id: string;

  @Column()
  f_number_id: string;

  @Column()
  position_id: string;

  @Column()
  name: string;

  @Column()
  cpf: string;

  @Column()
  stamp: number;

  @Column()
  rqs: string;

  @Column()
  wps: string;

  @Column()
  code: string;

  @Column()
  code_edition: string;

  @Column()
  base_metal: string;

  @Column()
  thickness: number;

  @Column()
  observation: string;

  @Column()
  process_type: number;

  @Column()
  backing: boolean;

  @Column()
  thickness_gw: number;

  @Column()
  thickness_fw: number;

  @Column()
  diameter_groove_weld: number;

  @Column()
  diameter_filled_weld: number;

  @Column()
  specification: string;

  @Column()
  classification: string;

  @Column()
  progression: number;

  @Column()
  shielding_gas: string;

  @Column()
  backing_gas: boolean;

  @Column()
  eletrical_ckp: number;

  @Column()
  transfer_mode: number;

  @Column()
  result_ve: boolean;

  @Column()
  test_type: number;

  @Column()
  executant: string;

  @Column()
  report: string;

  @Column()
  result_mvt: boolean;

  @Column()
  date: Date;

  @OneToOne(() => ProcessType, { eager: false })
  @JoinColumn({ name: 'welding_process_id' })
  welding_process: ProcessType;

  @OneToOne(() => PNumber, { eager: false })
  @JoinColumn({ name: 'p_number_id' })
  p_number: PNumber;

  @OneToOne(() => FNumber, { eager: false })
  @JoinColumn({ name: 'f_number_id' })
  f_number: FNumber;

  @OneToOne(() => Position, { eager: false })
  @JoinColumn({ name: 'position_id' })
  position: Position;

  @OneToMany(() => WQCC, wqcc => wqcc.welder, { eager: true })
  wqccs: WQCC[];

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;
}

export { Welder };
