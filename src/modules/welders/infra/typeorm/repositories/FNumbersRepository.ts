import { Repository, getRepository } from 'typeorm';

import { ICreateFNumberDTO } from '@modules/welders/dtos/ICreateFNumberDTO';
import { IFNumbersRepository } from '@modules/welders/repositories/IFNumbersRepository';
import { FNumber } from '../entities/FNumber';

class FNumbersRepository implements IFNumbersRepository {
  private repository: Repository<FNumber>;

  constructor() {
    this.repository = getRepository(FNumber);
  }

  async create(data: ICreateFNumberDTO): Promise<FNumber> {
    const fNumber = this.repository.create(data);

    return this.repository.save(fNumber);
  }

  async list(): Promise<FNumber[]> {
    return this.repository.find();
  }
}

export { FNumbersRepository };
