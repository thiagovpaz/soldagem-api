import { Repository, getRepository } from 'typeorm';

import { ICreateProcessTypeDTO } from '@modules/welders/dtos/ICreateProcessTypeDTO';
import { IProcessTypesRepository } from '@modules/welders/repositories/IProcessTypesRepository';
import { ProcessType } from '../entities/ProcessType';

class ProcessTypesRepository implements IProcessTypesRepository {
  private repository: Repository<ProcessType>;

  constructor() {
    this.repository = getRepository(ProcessType);
  }

  async create(data: ICreateProcessTypeDTO): Promise<ProcessType> {
    const processType = this.repository.create(data);

    return this.repository.save(processType);
  }

  async list(): Promise<ProcessType[]> {
    return this.repository.find();
  }
}

export { ProcessTypesRepository };
