import { getRepository, Repository } from 'typeorm';

import { ICreateWQCCDTO } from '@modules/welders/dtos/ICreateWQCCDTO';
import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';
import { WQCC } from '../entities/WQCC';

class WQCCsRepository implements IWQCCsRepository {
  private repository: Repository<WQCC>;

  constructor() {
    this.repository = getRepository(WQCC);
  }

  async create(data: ICreateWQCCDTO): Promise<WQCC> {
    const wqcc = this.repository.create(data);

    await this.repository.save(wqcc);

    return wqcc;
  }

  async list(welder_id: string): Promise<WQCC[]> {
    return this.repository.find({
      where: {
        welder_id,
      },
      order: { item: 'ASC' },
    });
  }

  async findById(id: string): Promise<WQCC> {
    return this.repository.findOne(id);
  }

  async save(wqcc: WQCC): Promise<WQCC> {
    return this.repository.save(wqcc);
  }

  async delete(id: string): Promise<void> {
    await this.repository.delete(id);
  }
}

export { WQCCsRepository };
