import { getRepository, ILike, Repository } from 'typeorm';

import { ICreateWelderDTO } from '@modules/welders/dtos/ICreateWelderDTO';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { ISearchParamsDTO } from '@modules/welders/dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '@modules/welders/dtos/ISearchResponseDTO';
import { Welder } from '../entities/Welder';

class WeldersRepository implements IWeldersRepository {
  private repository: Repository<Welder>;

  constructor() {
    this.repository = getRepository(Welder);
  }

  async create(data: Partial<ICreateWelderDTO>): Promise<Welder> {
    const welder = this.repository.create(data);

    await this.repository.save(welder);

    return welder;
  }

  async save(welder: Welder): Promise<Welder> {
    return this.repository.save(welder);
  }

  async findById(welder_id: string, relations: string[]): Promise<Welder> {
    return this.repository.findOne(welder_id, {
      relations,
    });
  }

  async findByCPF(cpf: string): Promise<Welder> {
    return this.repository.findOne({ where: { cpf } });
  }

  async findByStamp(stamp: number): Promise<Welder> {
    return this.repository.findOne({ where: { stamp } });
  }

  async delete(welder_id: string): Promise<void> {
    await this.repository.delete({ id: welder_id });
  }

  async list({
    query,
    page,
    per_page,
    user_id,
  }: ISearchParamsDTO): Promise<ISearchResponseDTO> {
    let queryParams = {};

    if (query) {
      queryParams = [
        { name: ILike(`%${query}%`) },
        { cpf: ILike(`%${query}%`) },
      ];
    }
    if (user_id)
      if (Array.isArray(queryParams)) {
        queryParams[0] = { ...queryParams[0], user_id };
        queryParams[1] = { ...queryParams[1], user_id };
      } else {
        queryParams = { user_id };
      }

    console.log(queryParams);

    const [welders, total] = await this.repository.findAndCount({
      where: queryParams,
      skip: page - 1,
      take: per_page,
      relations: ['welding_process', 'p_number', 'f_number', 'position'],
    });

    return { welders, total };
  }
}

export { WeldersRepository };
