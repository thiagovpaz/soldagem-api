import { Repository, getRepository } from 'typeorm';

import { ICreatePositionDTO } from '@modules/welders/dtos/ICreatePositionDTO';
import { IPositionsRepository } from '@modules/welders/repositories/IPositionsRepository';
import { Position } from '../entities/Position';

class PositionsRepository implements IPositionsRepository {
  private repository: Repository<Position>;

  constructor() {
    this.repository = getRepository(Position);
  }

  async create(data: ICreatePositionDTO): Promise<Position> {
    const position = this.repository.create(data);

    return this.repository.save(position);
  }

  async list(): Promise<Position[]> {
    return this.repository.find();
  }
}

export { PositionsRepository };
