import { Repository, getRepository } from 'typeorm';

import { ICreatePNumberDTO } from '@modules/welders/dtos/ICreatePNumberDTO';
import { IPNumbersRepository } from '@modules/welders/repositories/IPNumbersRepository';
import { PNumber } from '../entities/PNumber';

class PNumbersRepository implements IPNumbersRepository {
  private repository: Repository<PNumber>;

  constructor() {
    this.repository = getRepository(PNumber);
  }

  async create(data: ICreatePNumberDTO): Promise<PNumber> {
    const pNumber = this.repository.create(data);

    return this.repository.save(pNumber);
  }

  async list(): Promise<PNumber[]> {
    return this.repository.find();
  }
}

export { PNumbersRepository };
