import { Router } from 'express';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';

import { GenerateWPQController } from '@modules/welders/useCases/generateWPQ/GenerateWPQController';
import { GenerateWQLController } from '@modules/welders/useCases/generateWQL/GenerateWQLController';
import { CreateWelderController } from '@modules/welders/useCases/createWelder/CreateWelderController';
import { ListWeldersController } from '@modules/welders/useCases/listWelders/ListWeldersController';
import { DeleteWelderController } from '@modules/welders/useCases/deleteWelder/DeleteWelderController';
import { GetWelderController } from '@modules/welders/useCases/getWelder/GetWelderController';
import { UpdateWelderController } from '@modules/welders/useCases/updateWelder/UpdateWelderController';
import { ListProcessesTypesController } from '@modules/welders/useCases/listProcessesTypes/ListProcessesTypesController';
import { ListPNumbersController } from '@modules/welders/useCases/listPNumbers/ListPNumbersController';
import { ListFNumbersController } from '@modules/welders/useCases/listFNumbers/ListFNumbersController';
import { ListPositionsController } from '@modules/welders/useCases/listPositions/ListPositionsController';
import { CreateWQCCController } from '@modules/welders/useCases/createWQCC/CreateWQCCController';
import { ListWQCCController } from '@modules/welders/useCases/listWQCC/ListWQCCController';
import { DeleteWQCCController } from '@modules/welders/useCases/deleteWQCC/DeleteWQCCController';
import { UpdateWQCCController } from '@modules/welders/useCases/updateWQCC/UpdateWQCCController';
import { SearchStampController } from '@modules/welders/useCases/searchStamp/SearchStampController';

const weldersRouter = Router();

const generateWPQController = new GenerateWPQController();
const generateWQLController = new GenerateWQLController();
const createWelderController = new CreateWelderController();
const listWeldersController = new ListWeldersController();
const deleteWelderController = new DeleteWelderController();
const getWelderController = new GetWelderController();
const updateWelderController = new UpdateWelderController();
const listProcessesTypesController = new ListProcessesTypesController();
const listPNumbersController = new ListPNumbersController();
const listFNumbersController = new ListFNumbersController();
const listPositionsController = new ListPositionsController();
const createWQCCController = new CreateWQCCController();
const listWQCCController = new ListWQCCController();
const deleteWQCCController = new DeleteWQCCController();
const updateWQCCController = new UpdateWQCCController();
const searchStampController = new SearchStampController();

weldersRouter.use(ensureAuthenticated);

weldersRouter.post('/stamp', searchStampController.handle);

weldersRouter.post('/wpq/generate', generateWPQController.handle);
weldersRouter.post('/wql/generate', generateWQLController.handle);
weldersRouter.get('/processes_types/list', listProcessesTypesController.handle);
weldersRouter.get('/pnumbers/list', listPNumbersController.handle);
weldersRouter.get('/fnumbers/list', listFNumbersController.handle);
weldersRouter.get('/positions/list', listPositionsController.handle);
weldersRouter.get('/:welder_id/wqcc', listWQCCController.handle);
weldersRouter.post('/:welder_id/wqcc', createWQCCController.handle);
weldersRouter.delete('/:wqcc_id/wqcc', deleteWQCCController.handle);
weldersRouter.put('/:wqcc_id/wqcc', updateWQCCController.handle);

weldersRouter.post('/', createWelderController.handle);
weldersRouter.get('/', listWeldersController.handle);
weldersRouter.delete('/', deleteWelderController.handle);
weldersRouter.get('/:welder_id', getWelderController.handle);
weldersRouter.put('/:welder_id', updateWelderController.handle);

export { weldersRouter };
