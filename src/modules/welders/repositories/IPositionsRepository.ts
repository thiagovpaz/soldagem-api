import { ICreatePositionDTO } from '../dtos/ICreatePositionDTO';
import { Position } from '../infra/typeorm/entities/Position';

interface IPositionsRepository {
  create(data: ICreatePositionDTO): Promise<Position>;
  list(): Promise<Position[]>;
}

export { IPositionsRepository };
