import { ICreateProcessTypeDTO } from '@modules/welders/dtos/ICreateProcessTypeDTO';
import { ProcessType } from '@modules/welders/infra/typeorm/entities/ProcessType';
import { IProcessTypesRepository } from '../IProcessTypesRepository';

class ProcessTypesRepositoryInMemory implements IProcessTypesRepository {
  private process_types: ProcessType[] = [];

  async create(data: ICreateProcessTypeDTO): Promise<ProcessType> {
    const process_type = new ProcessType();

    Object.assign(process_type, data);

    this.process_types.push(process_type);

    return process_type;
  }

  async list(): Promise<ProcessType[]> {
    return this.process_types;
  }
}

export { ProcessTypesRepositoryInMemory };
