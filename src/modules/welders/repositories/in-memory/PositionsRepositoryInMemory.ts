import { ICreatePositionDTO } from '@modules/welders/dtos/ICreatePositionDTO';
import { Position } from '@modules/welders/infra/typeorm/entities/Position';
import { IPositionsRepository } from '../IPositionsRepository';

class PositionsRepositoryInMemory implements IPositionsRepository {
  private positions: Position[] = [];

  async create(data: ICreatePositionDTO): Promise<Position> {
    const position = new Position();

    Object.assign(position, data);

    this.positions.push(position);

    return position;
  }

  async list(): Promise<Position[]> {
    return this.positions;
  }
}

export { PositionsRepositoryInMemory };
