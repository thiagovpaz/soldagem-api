import { ICreateWelderDTO } from '@modules/welders/dtos/ICreateWelderDTO';
import { ISearchParamsDTO } from '@modules/welders/dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '@modules/welders/dtos/ISearchResponseDTO';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { IWeldersRepository } from '../IWeldersRepository';

class WeldersRepositoryInMemory implements IWeldersRepository {
  private welders: Welder[] = [];

  async create(data: Partial<ICreateWelderDTO>): Promise<Welder> {
    const welder = new Welder();

    Object.assign(welder, data);

    this.welders.push(welder);

    return welder;
  }

  async save(welder: Welder): Promise<Welder> {
    const findWelderIndex = this.welders.findIndex(w => w.id === welder.id);

    this.welders[findWelderIndex] = welder;

    return welder;
  }

  async findById(welder_id: string, _relations: string[]): Promise<Welder> {
    return this.welders.find(w => w.id === welder_id);
  }

  async findByCPF(cpf: string): Promise<Welder> {
    return this.welders.find(w => w.cpf === cpf);
  }

  async findByStamp(stamp: number): Promise<Welder> {
    return this.welders.find(w => w.stamp === stamp);
  }

  async delete(welder_id: string): Promise<void> {
    this.welders = this.welders.filter(w => w.id !== welder_id);
  }

  async list(_params: ISearchParamsDTO): Promise<ISearchResponseDTO> {
    return { welders: this.welders, total: this.welders.length };
  }
}

export { WeldersRepositoryInMemory };
