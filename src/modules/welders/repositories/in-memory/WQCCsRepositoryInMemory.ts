import { ICreateWQCCDTO } from '@modules/welders/dtos/ICreateWQCCDTO';
import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';
import { IWQCCsRepository } from '../IWQCCsRepository';

class WQCCsRepositoryInMemory implements IWQCCsRepository {
  private wqccs: WQCC[] = [];

  async create(data: ICreateWQCCDTO): Promise<WQCC> {
    const wqcc = new WQCC();

    Object.assign(wqcc, data);

    this.wqccs.push(wqcc);

    return wqcc;
  }

  async list(): Promise<WQCC[]> {
    return this.wqccs;
  }

  async findById(id: string): Promise<WQCC> {
    return this.wqccs.find(wqcc => wqcc.id === id);
  }

  async save(wqcc: WQCC): Promise<WQCC> {
    const findWqccIndex = this.wqccs.findIndex(w => w.id === wqcc.id);

    this.wqccs[findWqccIndex] = wqcc;

    return wqcc;
  }

  async delete(id: string): Promise<void> {
    this.wqccs = this.wqccs.filter(wqcc => wqcc.id !== id);
  }
}

export { WQCCsRepositoryInMemory };
