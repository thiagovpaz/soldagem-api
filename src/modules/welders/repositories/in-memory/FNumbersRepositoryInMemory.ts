import { ICreateFNumberDTO } from '@modules/welders/dtos/ICreateFNumberDTO';
import { FNumber } from '@modules/welders/infra/typeorm/entities/FNumber';
import { IFNumbersRepository } from '../IFNumbersRepository';

class FNumbersRepositoryInMemory implements IFNumbersRepository {
  private f_numbers: FNumber[] = [];

  async create(data: ICreateFNumberDTO): Promise<FNumber> {
    const f_number = new FNumber();

    Object.assign(f_number, data);

    this.f_numbers.push(f_number);

    return f_number;
  }

  async list(): Promise<FNumber[]> {
    return this.f_numbers;
  }
}

export { FNumbersRepositoryInMemory };
