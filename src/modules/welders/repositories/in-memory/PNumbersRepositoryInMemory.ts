import { ICreatePNumberDTO } from '@modules/welders/dtos/ICreatePNumberDTO';
import { PNumber } from '@modules/welders/infra/typeorm/entities/PNumber';
import { IPNumbersRepository } from '../IPNumbersRepository';

class PNumbersRepositoryInMemory implements IPNumbersRepository {
  private p_numbers: PNumber[] = [];

  async create(data: ICreatePNumberDTO): Promise<PNumber> {
    const p_number = new PNumber();

    Object.assign(p_number, data);

    this.p_numbers.push(p_number);

    return p_number;
  }

  async list(): Promise<PNumber[]> {
    return this.p_numbers;
  }
}

export { PNumbersRepositoryInMemory };
