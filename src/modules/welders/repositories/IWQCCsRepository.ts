import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';
import { ICreateWQCCDTO } from '../dtos/ICreateWQCCDTO';

interface IWQCCsRepository {
  create(data: ICreateWQCCDTO): Promise<WQCC>;
  list(welder_id: string): Promise<WQCC[]>;
  findById(id: string): Promise<WQCC>;
  save(wqcc: WQCC): Promise<WQCC>;
  delete(id: string): Promise<void>;
}

export { IWQCCsRepository };
