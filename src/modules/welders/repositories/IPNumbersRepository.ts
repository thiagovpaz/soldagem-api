import { ICreatePNumberDTO } from '../dtos/ICreatePNumberDTO';
import { PNumber } from '../infra/typeorm/entities/PNumber';

interface IPNumbersRepository {
  create(data: ICreatePNumberDTO): Promise<PNumber>;
  list(): Promise<PNumber[]>;
}

export { IPNumbersRepository };
