import { ICreateWelderDTO } from '../dtos/ICreateWelderDTO';
import { ISearchParamsDTO } from '../dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '../dtos/ISearchResponseDTO';
import { Welder } from '../infra/typeorm/entities/Welder';

interface IWeldersRepository {
  create(data: Partial<ICreateWelderDTO>): Promise<Welder>;
  save(welder: Welder): Promise<Welder>;
  findById(welder_id: string, relations?: string[]): Promise<Welder>;
  findByCPF(cpf: string): Promise<Welder>;
  findByStamp(stamp: number): Promise<Welder>;
  delete(welder_id: string): Promise<void>;
  list(params: ISearchParamsDTO): Promise<ISearchResponseDTO>;
}

export { IWeldersRepository };
