import { ICreateFNumberDTO } from '../dtos/ICreateFNumberDTO';
import { FNumber } from '../infra/typeorm/entities/FNumber';

interface IFNumbersRepository {
  create(data: ICreateFNumberDTO): Promise<FNumber>;
  list(): Promise<FNumber[]>;
}

export { IFNumbersRepository };
