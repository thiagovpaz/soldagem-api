import { ICreateProcessTypeDTO } from '../dtos/ICreateProcessTypeDTO';
import { ProcessType } from '../infra/typeorm/entities/ProcessType';

interface IProcessTypesRepository {
  create(data: ICreateProcessTypeDTO): Promise<ProcessType>;
  list(): Promise<ProcessType[]>;
}

export { IProcessTypesRepository };
