import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListPositionsUseCase } from './ListPositionsUseCase';

class ListPositionsController {
  async handle(request: Request, response: Response): Promise<Response> {
    const listPositionsUseCase = container.resolve(ListPositionsUseCase);

    const positions = await listPositionsUseCase.execute();

    return response.json(positions);
  }
}

export { ListPositionsController };
