import { PositionsRepositoryInMemory } from '@modules/welders/repositories/in-memory/PositionsRepositoryInMemory';
import { ListPositionsUseCase } from './ListPositionsUseCase';

let positionsRepositoryInMemory: PositionsRepositoryInMemory;
let listPositionsUseCase: ListPositionsUseCase;

describe('ListPositionsUseCase', () => {
  beforeEach(() => {
    positionsRepositoryInMemory = new PositionsRepositoryInMemory();

    listPositionsUseCase = new ListPositionsUseCase(
      positionsRepositoryInMemory,
    );
  });

  it('should be able to list all positions', async () => {
    await positionsRepositoryInMemory.create({
      test_position: 'fake-test-position',
      qualified_position: 'fake-qualified-position',
    });

    const response = await listPositionsUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          test_position: 'fake-test-position',
        }),
      ]),
    );
  });
});
