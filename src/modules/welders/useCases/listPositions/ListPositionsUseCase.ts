import { inject, injectable } from 'tsyringe';

import { IPositionsRepository } from '@modules/welders/repositories/IPositionsRepository';
import { Position } from '@modules/welders/infra/typeorm/entities/Position';

@injectable()
class ListPositionsUseCase {
  constructor(
    @inject('PositionsRepository')
    private positionsRepository: IPositionsRepository,
  ) {}
  async execute(): Promise<Position[]> {
    const positions = await this.positionsRepository.list();

    return positions;
  }
}

export { ListPositionsUseCase };
