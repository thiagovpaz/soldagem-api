import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { IWelderResponseDTO } from '@modules/welders/dtos/IWelderResponseDTO';

@injectable()
class GetWelderUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}

  async execute(user_id: string): Promise<IWelderResponseDTO> {
    const findWelder = await this.weldersRepository.findById(user_id, [
      'welding_process',
      'p_number',
      'f_number',
      'position',
    ]);

    if (!findWelder) {
      throw new AppError('Welder does not exists');
    }

    return findWelder;
  }
}

export { GetWelderUseCase };
