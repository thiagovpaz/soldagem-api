import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';
import { GetWelderUseCase } from './GetWelderUseCase';

let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let getWelderUseCase: GetWelderUseCase;

describe('GetWelderUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    getWelderUseCase = new GetWelderUseCase(weldersRepositoryInMemory);
  });

  it('should be able to get a welder by id', async () => {
    const welder = await weldersRepositoryInMemory.create({
      name: 'fake-name',
      cpf: 'fake-cpf',
    });

    const response = await getWelderUseCase.execute(welder.id);

    expect(response).toHaveProperty('id');
    expect(response.id).toBe(welder.id);
  });

  it('should not be able to get a welder with an invalid id', async () => {
    await expect(
      getWelderUseCase.execute('fake-invalid-welder-id'),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
