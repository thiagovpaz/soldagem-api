import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { GetWelderUseCase } from './GetWelderUseCase';

class GetWelderController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { welder_id } = request.params;

    const getWelderUseCase = container.resolve(GetWelderUseCase);

    const welder = await getWelderUseCase.execute(welder_id);

    return response.json(welder);
  }
}

export { GetWelderController };
