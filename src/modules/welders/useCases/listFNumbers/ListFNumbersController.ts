import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListFNumbersUseCase } from './ListFNumbersUseCase';

class ListFNumbersController {
  async handle(request: Request, response: Response): Promise<Response> {
    const listFNumbersUseCase = container.resolve(ListFNumbersUseCase);

    const fNumbers = await listFNumbersUseCase.execute();

    return response.json(fNumbers);
  }
}

export { ListFNumbersController };
