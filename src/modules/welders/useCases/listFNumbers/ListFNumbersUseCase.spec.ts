import { FNumbersRepositoryInMemory } from '@modules/welders/repositories/in-memory/FNumbersRepositoryInMemory';
import { ListFNumbersUseCase } from './ListFNumbersUseCase';

let fNumbersRepositoryInMemory: FNumbersRepositoryInMemory;
let listFNumbersUseCase: ListFNumbersUseCase;

describe('ListFNumbersUseCase', () => {
  beforeEach(() => {
    fNumbersRepositoryInMemory = new FNumbersRepositoryInMemory();

    listFNumbersUseCase = new ListFNumbersUseCase(fNumbersRepositoryInMemory);
  });

  it('should be able to list all f numbers', async () => {
    await fNumbersRepositoryInMemory.create({
      fn: 'fake-fn',
      qualified_range: 'fake-range',
    });

    const response = await listFNumbersUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          fn: 'fake-fn',
        }),
      ]),
    );
  });
});
