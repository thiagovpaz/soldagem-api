import { inject, injectable } from 'tsyringe';

import { IFNumbersRepository } from '@modules/welders/repositories/IFNumbersRepository';
import { FNumber } from '@modules/welders/infra/typeorm/entities/FNumber';

@injectable()
class ListFNumbersUseCase {
  constructor(
    @inject('FNumbersRepository')
    private pNumbersRepository: IFNumbersRepository,
  ) {}
  async execute(): Promise<FNumber[]> {
    const pNumbers = await this.pNumbersRepository.list();

    return pNumbers;
  }
}

export { ListFNumbersUseCase };
