import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';
import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';

interface IRequest {
  wqcc_id: string;
  data?: Partial<WQCC>;
}

@injectable()
class UpdateWQCCUseCase {
  constructor(
    @inject('WQCCsRepository')
    private wqccsRepository: IWQCCsRepository,
  ) {}

  async execute({ wqcc_id, data }: IRequest) {
    const wqcc = await this.wqccsRepository.findById(wqcc_id);

    if (!wqcc) {
      throw new AppError('Item does not exists');
    }

    Object.assign(wqcc, data);

    await this.wqccsRepository.save(wqcc);

    return wqcc;
  }
}

export { UpdateWQCCUseCase };
