import { AppError } from '@shared/errors/AppError';
import { WQCCsRepositoryInMemory } from '@modules/welders/repositories/in-memory/WQCCsRepositoryInMemory';
import { UpdateWQCCUseCase } from './UpdateWQCCUseCase';

let updateWQCCUseCase: UpdateWQCCUseCase;
let wqccsRepositoryInMemory: WQCCsRepositoryInMemory;

describe('DeleteWQCCUseCase', () => {
  beforeEach(() => {
    wqccsRepositoryInMemory = new WQCCsRepositoryInMemory();

    updateWQCCUseCase = new UpdateWQCCUseCase(wqccsRepositoryInMemory);
  });

  it('should be able to update a wqcc item', async () => {
    const updateWQCC = jest.spyOn(wqccsRepositoryInMemory, 'save');

    const { id: wqcc_id } = await wqccsRepositoryInMemory.create({
      welder_id: 'fake-welder-id',
      item: 1,
      date: new Date(),
      validity: new Date(),
      evidency_welding: 'fake-evidence-welding',
      validation_responsible: 'fake-responsible-validation',
    });

    await updateWQCCUseCase.execute({
      wqcc_id,
      data: { evidency_welding: 'fake-evidence-welding-updated' },
    });

    expect(updateWQCC).toHaveBeenCalled();
  });

  it('should not be able to update a wqcc with an invalid id', async () => {
    await expect(
      updateWQCCUseCase.execute({
        wqcc_id: 'fake-invalid-welder-id',
        data: { evidency_welding: 'fake-evidence-welding-updated' },
      }),
    ).rejects.toEqual(new AppError('Item does not exists'));
  });
});
