import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UpdateWQCCUseCase } from './UpdateWQCCUseCase';

class UpdateWQCCController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { wqcc_id } = request.params;
    const data = request.body;

    const updateWQCCUseCase = container.resolve(UpdateWQCCUseCase);

    const wqcc = await updateWQCCUseCase.execute({ wqcc_id, data });

    return response.json(wqcc);
  }
}

export { UpdateWQCCController };
