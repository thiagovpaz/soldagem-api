import { injectable, inject } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { IWelderDataProvider } from '@shared/container/providers/WelderDataProvider/IWelderDataProvider';
import { IWeldingVariablesType } from '@shared/container/providers/WelderDataProvider/implementations/WelderDataProvider';
import { IDateProvider } from '@shared/container/providers/DateProvider/IDateProvider';
import { ProcessType } from '@modules/welders/infra/typeorm/entities/ProcessType';

@injectable()
class GenerateWPQUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
    @inject('WelderDataProvider')
    private welderDataProvider: IWelderDataProvider,
    @inject('DayjsDateProvider')
    private dateProvider: IDateProvider,
  ) {}
  async execute(welders_ids: string[]): Promise<string> {
    try {
      const wp = this.welderDataProvider;

      wp.folderName = `${new Date().getTime()}`;
      wp.fileToOwner = [];

      const generate = welders_ids.map(async (wid: string) => {
        const welder = await this.weldersRepository.findById(wid, [
          'welding_process',
          'p_number',
          'f_number',
          'position',
        ]);

        if (!welder) {
          throw new AppError('Welder does not exists');
        }

        // wp.fileToOwner.push({ id: wid, name: welder.name, rqs: welder.rqs });
        wp.fileToOwner.push(welder);

        await wp.load('wpq-22.03.21-r2.xlsx');

        wp.setFields([
          {
            id: 'wpq',
            key: ['BC1', 'BC63'],
          },
          {
            id: 'date',
            key: 'BC3',
          },
          {
            id: 'name',
            key: ['H6', 'H68'],
          },
          {
            id: 'cpf',
            key: ['AP6', 'AP68'],
          },
          {
            id: 'stamp',
            key: ['BG6', 'BG68'],
          },
          {
            id: 'wps',
            key: 'D8',
          },
          {
            id: 'code',
            key: ['U8', 'AZ70'],
          },
          {
            id: 'code',
            key: 'U8',
          },
          {
            id: 'base_metal',
            key: 'AO8',
          },
          {
            id: 'thickness',
            key: 'BG8',
          },
          {
            id: 'process',
            key: 'H70',
          },
          {
            id: 'process_type',
            key: 'AD70',
          },
          {
            id: 'result_ve',
            key: 'F55',
          },
          {
            id: 'test_type',
            key: 'P55',
          },
          {
            id: 'executant',
            key: 'AD55',
          },
          {
            id: 'report',
            key: 'AS55',
          },
          {
            id: 'result_mvt',
            key: 'BE55',
          },
          {
            id: 'observation',
            key: 'A48',
          },
        ]);

        wp.setCellValue({
          id: 'wpq',
          value: welder.rqs,
        });

        wp.setCellValue({
          id: 'date',
          value: this.dateProvider.convert(welder.date),
          formatter: 'date',
        });

        wp.setCellValue({
          id: 'name',
          value: welder.name,
        });

        wp.setCellValue({
          id: 'cpf',
          value: welder.cpf,
        });

        wp.setCellValue({
          id: 'stamp',
          value: welder.stamp,
        });

        wp.setCellValue({
          id: 'wps',
          value: welder.wps,
        });

        wp.setCellValue({
          id: 'code',
          value: `${welder.code} ${welder.code_edition}`,
        });

        wp.setCellValue({
          id: 'base_metal',
          value: welder.base_metal,
        });

        wp.setCellValue({
          id: 'thickness',
          value: welder.thickness / 1,
          formatter: 'mm',
        });

        wp.setCellValue({
          id: 'process',
          value: welder.welding_process?.process_name,
        });

        const testTypeList = [
          ['Dobramento', 'Folding'],
          ['Radiografia', 'Radiography'],
          ['Ultrassom', 'Ultrassonic'],
        ];

        wp.setCellValue({
          id: 'test_type',
          value: testTypeList[welder.test_type],
        });

        wp.setCellValue({
          id: 'executant',
          value: welder.executant,
        });

        wp.setCellValue({
          id: 'report',
          value: welder.report,
        });

        wp.setCellValue({
          id: 'result_ve',
          value: welder.result_ve
            ? ['Aprovado', 'Approved']
            : ['Reprovado', 'Reproved'],
        });

        wp.setCellValue({
          id: 'result_mvt',
          value: welder.result_mvt
            ? ['Aprovado', 'Approved']
            : ['Reprovado', 'Reproved'],
        });

        const transferModeTypeList = [
          ['N/A', 'N/A'],
          ['Curto-circuito', 'Short circuit'],
          [
            'Spray, Globular e/ou Arco Pulsado',
            'Spray, Globular and/or Pulsed Arc',
          ],
        ];

        const eletricalCKPTypeList = ['CC-', 'CC+'];

        const progressionTypeList = [
          ['N/A', 'N/A'],
          ['Ascendente', 'Uphill'],
          ['Descendente', 'Downhill'],
        ];

        function validateWP(welding_process: string) {
          const range = ['FCAW', 'GMAW'];
          if (range.includes(welding_process)) {
            return 'FCAW / GMAW';
          }

          return welding_process;
        }

        function validatePT(pt: number) {
          const processTypeList = [
            ['Manual', 'Manual'],
            ['Automático', 'Automatic'],
          ];
          return processTypeList[pt];
        }

        function validatePTR(pt: ProcessType) {
          if (pt) return [...pt.process_type.split('|')];

          return null;
        }

        function validateBacking(backing: boolean) {
          if (backing === true) {
            return ['Com', 'With'];
          }
          return ['Com ou sem cobre-junta', 'With or without backing'];
        }

        function validateDGW(diameter: number) {
          if (diameter <= 25) {
            return `Ø ≥ ${diameter} mm`;
          }

          if (diameter > 25 && diameter < 73) {
            return `Ø > 25 mm`;
          }

          return `Ø ≥ ${diameter} mm`;
        }

        function validateTM(transfer_mode: number) {
          return transferModeTypeList[transfer_mode];
        }

        wp.setCellValue({
          id: 'process_type',
          value: validatePTR(welder.welding_process),
        });

        wp.setCellValue({
          id: 'observation',
          value: welder.observation,
        });

        const wv: IWeldingVariablesType[] = [
          {
            field: 'welding_process',
            title: ['Processo de Soldagem:', 'Welding Process'],
            params: welder.welding_process?.process_name,
            range: validateWP(welder.welding_process?.process_name),
          },
          {
            field: 'process_type',
            title: ['Tipo de Processo:', 'Process Type'],
            params: validatePT(welder.process_type),
            range: validatePTR(welder.welding_process),
          },
          {
            field: 'backing',
            title: ['Cobre Junta:', 'Backing'],
            params: welder.backing ? ['Com', 'With'] : ['Sem', 'Without'],
            range: validateBacking(welder.backing),
          },
          {
            field: 'pn',
            title: ['P Número', 'P Number'],
            params: welder.p_number?.pn,
            range:
              welder.p_number?.qualified_range.split('|').length > 1
                ? [...welder.p_number.qualified_range.split('|')]
                : welder.p_number?.qualified_range,
          },
          {
            field: 'thickness_gw',
            title: ['Espessura - Solda em chanfro', 'Thickness - Groove weld'],
            params: `${String(welder.thickness).replace('.', ',')} mm`,
            range:
              welder.thickness >= 13
                ? ['Todas', 'All']
                : ` ≤ ${String(welder.thickness * 2).replace('.', ',')} mm`,
          },
          {
            field: 'thickness_fw',
            title: ['Espessura - Solda em ângulo', 'Thickness - Fillet weld'],
            params: 'N.A.',
            range: ['Todas', 'All'],
          },
          {
            field: 'diameter_groove_weld',
            title: ['Diâmetro - Solda em chanfro', 'Diameter - Groove weld'],
            params: `${String(welder.diameter_groove_weld).replace(
              '.',
              ',',
            )} mm`,
            range: validateDGW(welder.diameter_groove_weld),
          },
          {
            field: 'diameter_filled_weld',
            title: ['Diâmetro - Solda em ângulo', 'Diameter - Fillet weld'],
            params: 'N.A.',
            range: ['Todos', 'All'],
          },
          {
            field: 'specification',
            title: ['Especificação (SFA)', 'Specification'],
            params: welder.specification,
            range: ['Ver Fnº, abaixo', 'See F number bellow'],
          },
          {
            field: 'classification',
            title: ['Classificação AWS', 'AWS Classification'],
            params: welder.classification,
            range: ['Ver Fnº, abaixo', 'See F number bellow'],
          },
          {
            field: 'fn',
            title: ['F Número', 'F Number'],
            params: welder.f_number?.fn,
            range:
              welder.f_number?.qualified_range.split('|').length > 1
                ? [...welder.f_number.qualified_range.split('|')]
                : welder.f_number?.qualified_range,
          },
          {
            field: 'position',
            title: ['Posição', 'Position'],
            params: welder.position?.test_position,
            range: ['Todas', 'All'],
          },
          {
            field: 'progression',
            title: ['Posição', 'Position'],
            params: progressionTypeList[welder.progression],
            range: progressionTypeList[welder.progression],
          },
          {
            field: 'shielding_gas',
            title: ['Gás de Proteção', 'Shielding gas'],
            params: [...welder.shielding_gas.split('|')],
            range: ['Qualquer', 'Any'],
          },
          {
            field: 'backing_gas',
            title: ['Gás de Purga', 'Backing gas'],
            params: welder.backing_gas ? ['Com', 'With'] : ['Sem', 'Without'],
            range: welder.backing_gas
              ? ['Com', 'With']
              : ['Com ou sem gás de purga', 'With or without backing gas'],
          },
          {
            field: 'eletrical_ckp',
            title: [
              'Tipo e Polaridade da corrente elétrica',
              'Eletrical current kind and polarity',
            ],
            params: eletricalCKPTypeList[welder.eletrical_ckp],
            range:
              welder.welding_process?.process_name === 'SMAW'
                ? 'CC+/CA'
                : eletricalCKPTypeList[welder.eletrical_ckp],
          },
          {
            field: 'transfer_mode',
            title: ['Modo de transferência', 'Transfer mode'],
            params: transferModeTypeList[welder.transfer_mode],
            range: validateTM(welder.transfer_mode),
          },
        ];

        wp.addWV(wv);

        if (welder.wqccs) {
          welder.wqccs.forEach(wqcc => {
            const { evidency_welding, validation_responsible, date, validity } =
              wqcc;

            wp.addWQCC({
              evidency_of_welding: evidency_welding,
              date: this.dateProvider.convert(date),
              validity: this.dateProvider.convert(validity),
              responsible_for_validation: validation_responsible,
            });
          });
        }

        const base_url = `${process.env.APP_FRONT_URL}/welders/${welder.id}`;
        wp.addHyperlink(base_url);

        await wp.save(`${wid}`);
      });

      await Promise.all(generate);

      const fileGenerated = await wp.export(welders_ids);

      await wp.clean();

      return fileGenerated;
    } catch (error) {
      throw new AppError(error.message);
    }
  }
}

export { GenerateWPQUseCase };
