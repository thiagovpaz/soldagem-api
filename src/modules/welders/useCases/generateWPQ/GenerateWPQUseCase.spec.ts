import { WelderDataProviderInMemory } from '@shared/container/providers/WelderDataProvider/in-memory/WelderDataProviderInMemory';
import { DayjsDateProvider } from '@shared/container/providers/DateProvider/implementations/DayjsDateProvider';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { GenerateWPQUseCase } from './GenerateWPQUseCase';

let generateWPQUseCase: GenerateWPQUseCase;
let dayjsDateProvider: DayjsDateProvider;
let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let welderDataProviderInMemory: WelderDataProviderInMemory;

describe('GenerateWPQUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();
    welderDataProviderInMemory = new WelderDataProviderInMemory();

    dayjsDateProvider = new DayjsDateProvider();

    generateWPQUseCase = new GenerateWPQUseCase(
      weldersRepositoryInMemory,
      welderDataProviderInMemory,
      dayjsDateProvider,
    );
  });

  it('should be able to generate a WPQ for a welder', async () => {
    const base_welder: Partial<Welder> = {
      name: 'OSMAR JOSE DA SILVA',
      cpf: '022.931.669-70',
      stamp: 1,
      rqs: '1-22',
      wps: 'UTM-EPS-01',
      code: 'ASME IX',
      code_edition: '2021',
      base_metal: 'A-106 Gr B',
      thickness: 13,
      observation: 'Nenhuma',
      result_ve: true,
      executant: 'Maicon R. Scheid',
      report: '2022/US181',
      result_mvt: true,
      process_type: 1,
      test_type: 0,
      backing: true,
      thickness_gw: 6.35,
      thickness_fw: 0,
      diameter_groove_weld: 75,
      diameter_filled_weld: 0,
      specification: 'A 5.18',
      classification: 'ER70S-3',
      progression: 1,
      shielding_gas: 'Argônio | Argon',
      backing_gas: true,
      eletrical_ckp: 0,
      transfer_mode: 0,
      wqccs: [],
      date: new Date(),
    };

    const { id: id1 } = await weldersRepositoryInMemory.create(base_welder);

    const { id: id2 } = await weldersRepositoryInMemory.create({
      ...base_welder,
      diameter_groove_weld: 10,
    });

    const { id: id3 } = await weldersRepositoryInMemory.create({
      ...base_welder,
      p_number: {
        qualified_range: 'fake-range',
      },
      f_number: {
        qualified_range: 'fake-range | fake-range-translate',
      },
    });

    const load = jest.spyOn(welderDataProviderInMemory, 'load');

    await generateWPQUseCase.execute([id1]);
    await generateWPQUseCase.execute([id2]);
    await generateWPQUseCase.execute([id3]);

    expect(load).toHaveBeenCalled();
  });

  it('should not be able to generate with an invalid welder', async () => {
    await expect(
      generateWPQUseCase.execute(['fake-invalid-welder-id']),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
