import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { GenerateWPQUseCase } from './GenerateWPQUseCase';

class GenerateWPQController {
  async handle(request: Request, response: Response): Promise<void> {
    const { welders_ids } = request.body;

    const generateWPQUseCase = container.resolve(GenerateWPQUseCase);

    const filename = await generateWPQUseCase.execute(welders_ids);

    return response.download(filename);
  }
}

export { GenerateWPQController };
