import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { DeleteWelderUseCase } from './DeleteWelderUseCase';

class DeleteWelderController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { welders_ids } = request.body;

    const deleteWelderUseCase = container.resolve(DeleteWelderUseCase);

    await deleteWelderUseCase.execute(welders_ids);

    return response.status(204).json({});
  }
}

export { DeleteWelderController };
