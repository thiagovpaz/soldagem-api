import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';

@injectable()
class DeleteWelderUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}

  async execute(welder_ids: string[]): Promise<void> {
    const removeAll = welder_ids.map(async welder_id => {
      const findWelder = await this.weldersRepository.findById(welder_id);

      if (!findWelder) {
        throw new AppError('Welder does not exists');
      }

      await this.weldersRepository.delete(welder_id);
    });

    await Promise.all(removeAll);
  }
}

export { DeleteWelderUseCase };
