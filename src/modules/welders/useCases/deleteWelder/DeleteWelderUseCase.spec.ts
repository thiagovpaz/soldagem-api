import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';
import { DeleteWelderUseCase } from './DeleteWelderUseCase';

let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let deleteWelderUseCase: DeleteWelderUseCase;

describe('DeleteWelderUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();
    deleteWelderUseCase = new DeleteWelderUseCase(weldersRepositoryInMemory);
  });

  it('should be able to delete a welder', async () => {
    const deletWelder = jest.spyOn(weldersRepositoryInMemory, 'delete');

    const { id } = await weldersRepositoryInMemory.create({
      name: 'fake-name',
      cpf: 'fake-cpf',
    });

    await deleteWelderUseCase.execute([id]);

    expect(deletWelder).toHaveBeenCalledWith(id);
  });

  it('should not be able to delete an invalid welder', async () => {
    await expect(
      deleteWelderUseCase.execute(['fake-invalid-welder-id']),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
