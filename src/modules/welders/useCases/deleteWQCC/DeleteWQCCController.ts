import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { DeleteWQCCUseCase } from './DeleteWQCCUseCase';

class DeleteWQCCController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { wqcc_id } = request.params;

    const deleteWQCCUseCase = container.resolve(DeleteWQCCUseCase);

    await deleteWQCCUseCase.execute(wqcc_id);

    return response.status(204).json({});
  }
}

export { DeleteWQCCController };
