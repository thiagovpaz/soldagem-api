import { AppError } from '@shared/errors/AppError';
import { WQCCsRepositoryInMemory } from '@modules/welders/repositories/in-memory/WQCCsRepositoryInMemory';
import { DeleteWQCCUseCase } from './DeleteWQCCUseCase';

let deleteWQCCUseCase: DeleteWQCCUseCase;
let wqccsRepositoryInMemory: WQCCsRepositoryInMemory;

describe('DeleteWQCCUseCase', () => {
  beforeEach(() => {
    wqccsRepositoryInMemory = new WQCCsRepositoryInMemory();

    deleteWQCCUseCase = new DeleteWQCCUseCase(wqccsRepositoryInMemory);
  });

  it('should be able to delete a wqcc item', async () => {
    const deletWQCC = jest.spyOn(wqccsRepositoryInMemory, 'delete');

    const { id: wqcc_id } = await wqccsRepositoryInMemory.create({
      welder_id: 'fake-welder-id',
      item: 1,
      date: new Date(),
      validity: new Date(),
      evidency_welding: 'fake-evidence-welding',
      validation_responsible: 'fake-responsible-validation',
    });

    await deleteWQCCUseCase.execute(wqcc_id);

    expect(deletWQCC).toHaveBeenCalledWith(wqcc_id);
  });

  it('should not be able to update a wqcc with an invalid id', async () => {
    await expect(
      deleteWQCCUseCase.execute('fake-invalid-welder-id'),
    ).rejects.toEqual(new AppError('Item does not exists'));
  });
});
