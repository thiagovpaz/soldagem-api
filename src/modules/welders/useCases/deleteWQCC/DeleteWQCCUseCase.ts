import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';

@injectable()
class DeleteWQCCUseCase {
  constructor(
    @inject('WQCCsRepository')
    private wqccsRepository: IWQCCsRepository,
  ) {}

  async execute(id: string): Promise<void> {
    const findWQCC = await this.wqccsRepository.findById(id);

    if (!findWQCC) {
      throw new AppError('Item does not exists');
    }

    await this.wqccsRepository.delete(id);
  }
}

export { DeleteWQCCUseCase };
