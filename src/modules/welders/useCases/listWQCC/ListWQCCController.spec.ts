import request from 'supertest';
import { Connection } from 'typeorm';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import { CreateWelder } from '@shared/infra/typeorm/seeds/welder.seed';
import { CreateWQCCs } from '@shared/infra/typeorm/seeds/wqcc.seed';
import { CreateFNumbers } from '@shared/infra/typeorm/seeds/f_number.seed';
import { CreatePositions } from '@shared/infra/typeorm/seeds/position.seed';
import { CreateProcessTypes } from '@shared/infra/typeorm/seeds/process_type.seed';
import { CreatePNumbers } from '@shared/infra/typeorm/seeds/p_number.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'secret',
};

let connection: Connection;

describe('GET /welders/:welder_id/wqcc', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
    await runSeeder(CreateProcessTypes);
    await runSeeder(CreatePNumbers);
    await runSeeder(CreateFNumbers);
    await runSeeder(CreatePositions);
    await runSeeder(CreateWelder);
    await runSeeder(CreateWQCCs);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to list all WQCCs', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const response = await request(app)
      .get('/v1/welders/4d370e58-dad4-405b-8c95-7e5ce9c4c9bb/wqcc')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(200);
    expect(response.body).toHaveLength(1);
  });
});
