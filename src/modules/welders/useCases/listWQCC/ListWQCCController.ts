import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListWQCCUseCase } from './ListWQCCUseCase';

class ListWQCCController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { welder_id } = request.params;

    const listWQCCUseCase = container.resolve(ListWQCCUseCase);

    const wqccs = await listWQCCUseCase.execute(welder_id);

    return response.json(wqccs);
  }
}

export { ListWQCCController };
