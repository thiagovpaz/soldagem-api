import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';

@injectable()
class ListWQCCUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
    @inject('WQCCsRepository')
    private wqccsRepository: IWQCCsRepository,
  ) {}

  async execute(welder_id: string): Promise<WQCC[]> {
    const findWelder = await this.weldersRepository.findById(welder_id);

    if (!findWelder) {
      throw new AppError('Welder does not exists');
    }

    const wqccs = await this.wqccsRepository.list(welder_id);

    return wqccs;
  }
}

export { ListWQCCUseCase };
