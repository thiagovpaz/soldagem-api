import { AppError } from '@shared/errors/AppError';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { WQCCsRepositoryInMemory } from '@modules/welders/repositories/in-memory/WQCCsRepositoryInMemory';
import { ListWQCCUseCase } from './ListWQCCUseCase';

let wqccsRepositoryInMemory: WQCCsRepositoryInMemory;
let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let listWQCCUseCase: ListWQCCUseCase;

describe('ListWQCCUseCase', () => {
  beforeEach(() => {
    wqccsRepositoryInMemory = new WQCCsRepositoryInMemory();
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    listWQCCUseCase = new ListWQCCUseCase(
      weldersRepositoryInMemory,
      wqccsRepositoryInMemory,
    );
  });

  it('should be able to list all WQCCs', async () => {
    const { id: welder_id } = await weldersRepositoryInMemory.create({
      name: 'fake-welder',
    });

    await wqccsRepositoryInMemory.create({
      welder_id,
      item: 1,
      evidency_welding: 'fake-evidence-welding',
      date: new Date(),
      validity: new Date(),
      validation_responsible: 'fake-evidence-welding',
    });

    const response = await listWQCCUseCase.execute(welder_id);

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          evidency_welding: 'fake-evidence-welding',
        }),
      ]),
    );
  });

  it('should not be able to list WQCCs', async () => {
    await expect(
      listWQCCUseCase.execute('fake-invalid-welder-id'),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
