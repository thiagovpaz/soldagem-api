import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { SearchStampUseCase } from './SearchStampUseCase';

class SearchStampController {
  async handle(request: Request, response: Response) {
    const { stamp } = request.body;

    const searchStampUseCase = container.resolve(SearchStampUseCase);
    await searchStampUseCase.execute(stamp);

    return response.status(204).json();
  }
}

export { SearchStampController };
