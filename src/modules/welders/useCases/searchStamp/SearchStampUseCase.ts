import { injectable, inject } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';

@injectable()
class SearchStampUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}

  async execute(stamp: number): Promise<void> {
    const findStamp = await this.weldersRepository.findByStamp(stamp);

    if (findStamp) {
      throw new AppError('This Stamp already exists.');
    }
  }
}

export { SearchStampUseCase };
