import { AppError } from '@shared/errors/AppError';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { SearchStampUseCase } from './SearchStampUseCase';

let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let searchStampUseCase: SearchStampUseCase;

describe('SearchStampUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    searchStampUseCase = new SearchStampUseCase(weldersRepositoryInMemory);
  });

  it('should be able to search a welder by stamp', async () => {
    await weldersRepositoryInMemory.create({
      stamp: 1,
    });

    await searchStampUseCase.execute(2);
  });

  it('should not be able to search a welder by stamp', async () => {
    await weldersRepositoryInMemory.create({
      stamp: 1,
    });

    await expect(searchStampUseCase.execute(1)).rejects.toEqual(
      new AppError('This Stamp already exists.'),
    );
  });
});
