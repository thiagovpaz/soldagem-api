import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWelderDataProvider } from '@shared/container/providers/WelderDataProvider/IWelderDataProvider';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';

@injectable()
class GenerateWQLUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
    @inject('WelderDataProvider')
    private welderDataProvider: IWelderDataProvider,
  ) {}
  async execute(user_id: string): Promise<void> {
    const user = await this.usersRepository.findById(user_id, [
      'welders',
      'welders.p_number',
      'welders.f_number',
      'welders.position',
      'welders.welding_process',
    ]);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const { welders } = user;

    const wp = this.welderDataProvider;

    wp.folderName = `${new Date().getTime()}`;

    await wp.addImage(user.avatar);

    await wp.load('wql-22.03.21-r2-with-logo.xlsx');

    wp.setFields([
      {
        id: 'customer',
        key: 'N1',
      },
      {
        id: 'date',
        key: 'N3',
      },
      {
        id: 'page',
        key: 'N4',
      },
      {
        id: 'work',
        key: 'B6',
      },
      {
        id: 'code',
        key: 'I6',
      },
      {
        id: 'revision',
        key: 'M6',
      },
      {
        id: 'stamp',
        key: 'A8',
      },
    ]);

    wp.setCellValue({
      id: 'customer',
      value: 'CXC / Satou',
    });

    wp.setCellValue({
      id: 'date',
      value: '24/03/2022',
      formatter: 'date',
    });

    wp.setCellValue({
      id: 'page',
      value: 1,
    });

    wp.setCellValue({
      id: 'work',
      value: 'CXC / Satou março/2022',
    });

    wp.setCellValue({
      id: 'code',
      value: 'ASME Section IX - ED. 2025',
    });

    wp.setCellValue({
      id: 'revision',
      value: 1,
    });

    if (welders) {
      welders.map(async w => {
        wp.WQLItems.push({
          stamp: String(w.stamp),
          wpq: null,
          name: w.name,
          process: w.welding_process?.process_name,
          pn: w.p_number?.pn,
          fn: w.f_number?.fn,
          position: w.position?.qualified_position,
          current_and_polarity: 'CC-',
          progression: String(w.progression),
          thickness: String(w.thickness),
          diameter: String(w.diameter_filled_weld),
          backing: String(w.backing),
          validity: '15/08/2022',
          wps: w.wps,
        });
      });

      wp.buildWQLItems();
    }

    await wp.save(user_id);
  }
}

export { GenerateWQLUseCase };
