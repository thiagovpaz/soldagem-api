import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { WelderDataProviderInMemory } from '@shared/container/providers/WelderDataProvider/in-memory/WelderDataProviderInMemory';
import { GenerateWQLUseCase } from './GenerateWQLUseCase';

let generateWQLUseCase: GenerateWQLUseCase;
let welderDataProviderInMemory: WelderDataProviderInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe('GenerateWQLUseCase', () => {
  beforeEach(() => {
    welderDataProviderInMemory = new WelderDataProviderInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    generateWQLUseCase = new GenerateWQLUseCase(
      usersRepositoryInMemory,
      welderDataProviderInMemory,
    );
  });

  it('should be able to generate a WQL', async () => {
    const load = jest.spyOn(welderDataProviderInMemory, 'load');

    const { id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    await generateWQLUseCase.execute(id);

    expect(load).toHaveBeenCalled();
  });

  it('should not be able to generate a WQL with an invalid user', async () => {
    await expect(generateWQLUseCase.execute('fake-invalid-id')).rejects.toEqual(
      new AppError('User does not exists'),
    );
  });
});
