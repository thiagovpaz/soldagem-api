import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { GenerateWQLUseCase } from './GenerateWQLUseCase';

class GenerateWQLController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;
    const generateWQLUseCase = container.resolve(GenerateWQLUseCase);

    await generateWQLUseCase.execute(user_id);

    return response.json();
  }
}

export { GenerateWQLController };
