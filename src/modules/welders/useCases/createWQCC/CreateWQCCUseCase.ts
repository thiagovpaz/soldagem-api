import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWQCCsRepository } from '@modules/welders/repositories/IWQCCsRepository';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { ICreateWQCCDTO } from '@modules/welders/dtos/ICreateWQCCDTO';
import { WQCC } from '@modules/welders/infra/typeorm/entities/WQCC';

@injectable()
class CreateWQCCUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
    @inject('WQCCsRepository')
    private wqccsRepository: IWQCCsRepository,
  ) {}

  async execute(data: ICreateWQCCDTO): Promise<WQCC> {
    const findWelder = await this.weldersRepository.findById(data.welder_id);

    if (!findWelder) {
      throw new AppError('Welder does not exists');
    }

    const wqcc = await this.wqccsRepository.create(data);

    return wqcc;
  }
}

export { CreateWQCCUseCase };
