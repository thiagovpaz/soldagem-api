import { AppError } from '@shared/errors/AppError';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { WQCCsRepositoryInMemory } from '@modules/welders/repositories/in-memory/WQCCsRepositoryInMemory';
import { CreateWQCCUseCase } from './CreateWQCCUseCase';

let wqccsRepositoryInMemory: WQCCsRepositoryInMemory;
let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let createWQCCUseCase: CreateWQCCUseCase;

describe('CreateWQCCUseCase', () => {
  beforeEach(() => {
    wqccsRepositoryInMemory = new WQCCsRepositoryInMemory();
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    createWQCCUseCase = new CreateWQCCUseCase(
      weldersRepositoryInMemory,
      wqccsRepositoryInMemory,
    );
  });

  it('should be able to create a WQCC', async () => {
    const { id: welder_id } = await weldersRepositoryInMemory.create({
      name: 'fake-welder',
    });

    const response = await createWQCCUseCase.execute({
      welder_id,
      item: 1,
      evidency_welding: 'fake-evidence-welding',
      date: new Date(),
      validity: new Date(),
      validation_responsible: 'fake-evidence-welding',
    });

    expect(response.evidency_welding).toBe('fake-evidence-welding');
  });

  it('should not be able to create a WQCC', async () => {
    await expect(
      createWQCCUseCase.execute({
        welder_id: 'fake-invalid-welder-id',
        item: 1,
        evidency_welding: 'fake-evidence-welding',
        date: new Date(),
        validity: new Date(),
        validation_responsible: 'fake-evidence-welding',
      }),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
