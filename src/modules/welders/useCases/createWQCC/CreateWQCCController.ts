import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateWQCCUseCase } from './CreateWQCCUseCase';

class CreateWQCCController {
  async handle(request: Request, response: Response): Promise<Response> {
    const data = request.body;
    const { welder_id } = request.params;

    const createWQCCUseCase = container.resolve(CreateWQCCUseCase);

    const wqcc = await createWQCCUseCase.execute({ ...data, welder_id });

    return response.status(201).json(wqcc);
  }
}

export { CreateWQCCController };
