import { inject, injectable } from 'tsyringe';

import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { ISearchParamsDTO } from '@modules/welders/dtos/ISearchParamsDTO';

interface IResponse {
  welders: Partial<Welder[]>;
  total: number;
}

@injectable()
class ListWeldersUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}
  async execute({
    query,
    page,
    per_page,
    user_id,
  }: ISearchParamsDTO): Promise<IResponse> {
    const { welders, total } = await this.weldersRepository.list({
      query,
      page,
      per_page,
      user_id,
    });

    return { total, welders };
  }
}

export { ListWeldersUseCase };
