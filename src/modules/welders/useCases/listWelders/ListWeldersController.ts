import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { instanceToPlain } from 'class-transformer';

import { ISearchParamsDTO } from '@modules/welders/dtos/ISearchParamsDTO';
import { ListWeldersUseCase } from './ListWeldersUseCase';

class ListWeldersController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { query, page, per_page, user_id } =
      request.query as ISearchParamsDTO;

    const listWeldersUseCase = container.resolve(ListWeldersUseCase);

    const { welders, total } = await listWeldersUseCase.execute({
      query: query || null,
      page: page || 1,
      per_page: per_page || 10,
      user_id: user_id || null,
    });

    return response.json({ total, welders: instanceToPlain(welders) });
  }
}

export { ListWeldersController };
