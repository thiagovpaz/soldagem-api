import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { ListWeldersUseCase } from './ListWeldersUseCase';

let listWeldersUseCase: ListWeldersUseCase;
let weldersRepositoryInMemory: WeldersRepositoryInMemory;

describe('ListWeldersUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    listWeldersUseCase = new ListWeldersUseCase(weldersRepositoryInMemory);
  });

  it('should be able to list all welders', async () => {
    await weldersRepositoryInMemory.create({
      name: 'fake-name',
      cpf: 'fake-cpf-password',
    });

    const response = await listWeldersUseCase.execute({});

    expect(response).toEqual(
      expect.objectContaining({
        welders: expect.arrayContaining([
          expect.objectContaining({
            name: 'fake-name',
          }),
        ]),
      }),
    );
  });
});
