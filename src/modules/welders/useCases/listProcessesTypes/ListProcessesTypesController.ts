import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListProcessesTypesUseCase } from './ListProcessesTypesUseCase';

class ListProcessesTypesController {
  async handle(request: Request, response: Response): Promise<Response> {
    const listProcessesTypesUseCase = container.resolve(
      ListProcessesTypesUseCase,
    );

    const processes = await listProcessesTypesUseCase.execute();

    return response.json(processes);
  }
}

export { ListProcessesTypesController };
