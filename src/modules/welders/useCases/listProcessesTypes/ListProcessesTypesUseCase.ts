import { inject, injectable } from 'tsyringe';

import { IProcessTypesRepository } from '@modules/welders/repositories/IProcessTypesRepository';
import { ProcessType } from '@modules/welders/infra/typeorm/entities/ProcessType';

@injectable()
class ListProcessesTypesUseCase {
  constructor(
    @inject('ProcessTypesRepository')
    private processTypesRepository: IProcessTypesRepository,
  ) {}
  async execute(): Promise<ProcessType[]> {
    const listProcessesTypes = await this.processTypesRepository.list();

    return listProcessesTypes;
  }
}

export { ListProcessesTypesUseCase };
