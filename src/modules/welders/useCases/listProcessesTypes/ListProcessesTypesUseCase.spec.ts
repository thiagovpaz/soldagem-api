import { ProcessTypesRepositoryInMemory } from '@modules/welders/repositories/in-memory/ProcessTypesRepositoryInMemory';
import { ListProcessesTypesUseCase } from './ListProcessesTypesUseCase';

let processTypesRepositoryInMemory: ProcessTypesRepositoryInMemory;
let listProcessesTypesUseCase: ListProcessesTypesUseCase;

describe('ListProcessesTypesUseCase', () => {
  beforeEach(() => {
    processTypesRepositoryInMemory = new ProcessTypesRepositoryInMemory();

    listProcessesTypesUseCase = new ListProcessesTypesUseCase(
      processTypesRepositoryInMemory,
    );
  });

  it('should be able to list all processes types', async () => {
    await processTypesRepositoryInMemory.create({
      process_name: 'fake-process-name',
      process_type: 'fake-process-type',
    });

    const response = await listProcessesTypesUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          process_name: 'fake-process-name',
        }),
      ]),
    );
  });
});
