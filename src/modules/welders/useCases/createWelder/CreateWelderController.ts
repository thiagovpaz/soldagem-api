import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateWelderUseCase } from './CreateWelderUseCase';

class CreateWelderController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;
    const data = request.body;
    const createWelderUseCase = container.resolve(CreateWelderUseCase);

    const welder = await createWelderUseCase.execute({
      user_id,
      ...data,
    });

    return response.status(201).json(welder);
  }
}

export { CreateWelderController };
