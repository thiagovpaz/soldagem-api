import { injectable, inject } from 'tsyringe';

import { ICreateWelderDTO } from '@modules/welders/dtos/ICreateWelderDTO';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';

@injectable()
class CreateWelderUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}
  async execute({ cpf, ...rest }: Partial<ICreateWelderDTO>): Promise<Welder> {
    return this.weldersRepository.create({ cpf, ...rest });
  }
}

export { CreateWelderUseCase };
