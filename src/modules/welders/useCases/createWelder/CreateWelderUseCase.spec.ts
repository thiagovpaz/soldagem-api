// import { AppError } from '@shared/errors/AppError';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { CreateWelderUseCase } from './CreateWelderUseCase';

let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let createWelderUseCase: CreateWelderUseCase;

describe('CreateWelderUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    createWelderUseCase = new CreateWelderUseCase(weldersRepositoryInMemory);
  });

  it('should be able to create a new welder', async () => {
    const response = await createWelderUseCase.execute({
      name: 'fake-welder',
      cpf: 'fake-welder-cpf',
    });

    expect(response.name).toBe('fake-welder');
  });

  // it('should not be able to create a new welder when cpf already exists', async () => {
  //   await weldersRepositoryInMemory.create({
  //     name: 'fake-welder',
  //     cpf: 'fake-welder-cpf',
  //   });

  //   await expect(
  //     createWelderUseCase.execute({
  //       name: 'fake-welder',
  //       cpf: 'fake-welder-cpf',
  //     }),
  //   ).rejects.toEqual(new AppError('Welder already exists'));
  // });
});
