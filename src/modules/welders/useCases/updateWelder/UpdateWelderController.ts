import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UpdateWelderUseCase } from './UpdateWelderUseCase';

class UpdateWelderController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { welder_id } = request.params;
    const data = request.body;

    const updateWelderUseCase = container.resolve(UpdateWelderUseCase);

    const welder = await updateWelderUseCase.execute({ welder_id, data });

    return response.json(welder);
  }
}

export { UpdateWelderController };
