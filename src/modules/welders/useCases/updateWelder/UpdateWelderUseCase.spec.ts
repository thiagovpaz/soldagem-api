import { AppError } from '@shared/errors/AppError';
import { WeldersRepositoryInMemory } from '@modules/welders/repositories/in-memory/WeldersRepositoryInMemory';
import { UpdateWelderUseCase } from './UpdateWelderUseCase';

let weldersRepositoryInMemory: WeldersRepositoryInMemory;
let updateWelderUseCase: UpdateWelderUseCase;

describe('UpdateWelderUseCase', () => {
  beforeEach(() => {
    weldersRepositoryInMemory = new WeldersRepositoryInMemory();

    updateWelderUseCase = new UpdateWelderUseCase(weldersRepositoryInMemory);
  });

  it('should be able to update a welder', async () => {
    const welder = await weldersRepositoryInMemory.create({
      name: 'fake-name',
    });

    const response = await updateWelderUseCase.execute({
      welder_id: welder.id,
      data: {
        name: 'fake-name-updated',
      },
    });

    expect(response).toHaveProperty('id');
    expect(response.name).toBe('fake-name-updated');
    expect(response.id).toBe(welder.id);
  });

  it('should not be able to update a welder with an invalid welder', async () => {
    await expect(
      updateWelderUseCase.execute({
        welder_id: 'fake-invalid-welder-id',
      }),
    ).rejects.toEqual(new AppError('Welder does not exists'));
  });
});
