import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IWeldersRepository } from '@modules/welders/repositories/IWeldersRepository';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { IWelderResponseDTO } from '@modules/welders/dtos/IWelderResponseDTO';

interface IRequest {
  welder_id: string;
  data?: Partial<Welder>;
}

@injectable()
class UpdateWelderUseCase {
  constructor(
    @inject('WeldersRepository')
    private weldersRepository: IWeldersRepository,
  ) {}

  async execute({ welder_id, data }: IRequest): Promise<IWelderResponseDTO> {
    const welder = await this.weldersRepository.findById(welder_id);

    if (!welder) {
      throw new AppError('Welder does not exists');
    }

    Object.assign(welder, data);

    await this.weldersRepository.save(welder);

    return welder;
  }
}

export { UpdateWelderUseCase };
