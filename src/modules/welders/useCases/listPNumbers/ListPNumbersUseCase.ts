import { inject, injectable } from 'tsyringe';

import { IPNumbersRepository } from '@modules/welders/repositories/IPNumbersRepository';
import { PNumber } from '@modules/welders/infra/typeorm/entities/PNumber';

@injectable()
class ListPNumbersUseCase {
  constructor(
    @inject('PNumbersRepository')
    private pNumbersRepository: IPNumbersRepository,
  ) {}
  async execute(): Promise<PNumber[]> {
    const pNumbers = await this.pNumbersRepository.list();

    return pNumbers;
  }
}

export { ListPNumbersUseCase };
