import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListPNumbersUseCase } from './ListPNumbersUseCase';

class ListPNumbersController {
  async handle(request: Request, response: Response): Promise<Response> {
    const listPNumbersUseCase = container.resolve(ListPNumbersUseCase);

    const pNumbers = await listPNumbersUseCase.execute();

    return response.json(pNumbers);
  }
}

export { ListPNumbersController };
