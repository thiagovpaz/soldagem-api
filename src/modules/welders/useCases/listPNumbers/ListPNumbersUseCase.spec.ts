import { PNumbersRepositoryInMemory } from '@modules/welders/repositories/in-memory/PNumbersRepositoryInMemory';
import { ListPNumbersUseCase } from './ListPNumbersUseCase';

let pNumbersRepositoryInMemory: PNumbersRepositoryInMemory;
let listPNumbersUseCase: ListPNumbersUseCase;

describe('ListPNumbersUseCase', () => {
  beforeEach(() => {
    pNumbersRepositoryInMemory = new PNumbersRepositoryInMemory();

    listPNumbersUseCase = new ListPNumbersUseCase(pNumbersRepositoryInMemory);
  });

  it('should be able to list all p numbers', async () => {
    await pNumbersRepositoryInMemory.create({
      pn: 'fake-pn',
      qualified_range: 'fake-range',
    });

    const response = await listPNumbersUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          pn: 'fake-pn',
        }),
      ]),
    );
  });
});
