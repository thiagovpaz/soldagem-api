import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';

interface ISearchResponseDTO {
  welders: Partial<Welder[]>;
  total: number;
}

export { ISearchResponseDTO };
