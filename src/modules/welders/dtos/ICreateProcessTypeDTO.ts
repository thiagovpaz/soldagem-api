interface ICreateProcessTypeDTO {
  process_type: string;
  process_name: string;
}

export { ICreateProcessTypeDTO };
