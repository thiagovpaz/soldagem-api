interface ICreatePNumberDTO {
  pn: string;
  qualified_range: string;
}

export { ICreatePNumberDTO };
