interface ICreateFNumberDTO {
  fn: string;
  qualified_range: string;
}

export { ICreateFNumberDTO };
