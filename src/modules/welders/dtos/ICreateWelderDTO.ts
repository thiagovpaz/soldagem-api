import { FNumber } from '../infra/typeorm/entities/FNumber';
import { PNumber } from '../infra/typeorm/entities/PNumber';

interface ICreateWelderDTO {
  user_id?: string;
  welding_process_id?: string;
  p_number_id?: string;
  f_number_id?: string;
  position_id?: string;

  p_number?: Partial<PNumber>;
  f_number?: Partial<FNumber>;

  name: string;
  cpf: string;
  stamp: number;
  rqs: string;
  wps: string;
  code: string;
  code_edition: string;
  base_metal: string;
  thickness: number;
  observation: string;

  process_type: number;
  backing: boolean;
  thickness_gw: number;
  thickness_fw: number;
  diameter_groove_weld: number;
  diameter_filled_weld: number;
  specification: string;
  classification: string;
  progression: number;
  shielding_gas: string;
  backing_gas: boolean;
  eletrical_ckp: number;
  transfer_mode: number;

  result_ve: boolean;
  test_type: number;
  executant: string;
  report: string;
  result_mvt: boolean;

  date: Date;
  created_at: Date;
  updated_at: Date;
}

export { ICreateWelderDTO };
