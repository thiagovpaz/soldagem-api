interface ISearchParamsDTO {
  query?: string | null;
  page?: number;
  per_page?: number;
  user_id?: string;
}
export { ISearchParamsDTO };
