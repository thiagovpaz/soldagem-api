interface ICreatePositionDTO {
  test_position: string;
  qualified_position: string;
}

export { ICreatePositionDTO };
