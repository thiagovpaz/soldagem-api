interface ICreateWQCCDTO {
  welder_id: string;
  item: number;
  evidency_welding: string;
  date: Date;
  validity: Date;
  validation_responsible: string;
}

export { ICreateWQCCDTO };
