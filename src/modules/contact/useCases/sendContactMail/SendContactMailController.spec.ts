import request from 'supertest';

import { app } from '@shared/infra/http/app';

jest.setTimeout(30000);

describe('POST /contact', () => {
  it('should be able to send a contact mail', async () => {
    const response = await request(app)
      .post('/v1/contact')
      .send({
        name: 'fake-name',
        email: 'fake@fake.com',
        phone: 'fake-phone',
        message: 'fake-message',
      })
      .set('Accept', 'application/json');

    expect(response.body.message).toEqual('Message sent');
    expect(response.status).toEqual(200);
  });
});
