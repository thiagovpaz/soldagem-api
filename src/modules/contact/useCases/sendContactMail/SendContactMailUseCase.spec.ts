import { MailProviderInMemory } from '@shared/container/providers/MailProvider/in-memory/MailProviderInMemory';
import { SendContactMailUseCase } from './SendContactMailUseCase';

let sendContactMailUseCase: SendContactMailUseCase;
let mailProvider: MailProviderInMemory;

describe('SendContactMailUseCase', () => {
  beforeEach(() => {
    mailProvider = new MailProviderInMemory();

    sendContactMailUseCase = new SendContactMailUseCase(mailProvider);
  });

  it('should be able to send a contact email', async () => {
    const fake_message = {
      name: 'fake-name',
      email: 'fake@fake.com',
      phone: 'fake-number',
      message: 'fake-message',
    };

    const sendMail = jest.spyOn(mailProvider, 'sendMail');

    await sendContactMailUseCase.execute(fake_message);

    expect(sendMail).toHaveBeenCalled();
  });
});
