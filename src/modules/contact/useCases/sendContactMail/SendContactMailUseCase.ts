import { inject, injectable } from 'tsyringe';
import { resolve } from 'path';

import { ICreateContactMailDTO } from '@modules/contact/dtos/ICreateContactMailDTO';
import { IMailProvider } from '@shared/container/providers/MailProvider/IMailProvider';

@injectable()
class SendContactMailUseCase {
  constructor(
    @inject('MailProvider')
    private mailProvider: IMailProvider,
  ) {}

  async execute({
    name,
    email,
    phone,
    message,
  }: ICreateContactMailDTO): Promise<void> {
    const templatePath = resolve(
      __dirname,
      '..',
      '..',
      'views',
      'emails',
      'contact.hbs',
    );

    const variables = {
      name,
      email,
      phone,
      message,
    };

    await this.mailProvider.sendMail(
      'programador@programador.me',
      'Contato',
      variables,
      templatePath,
    );
  }
}

export { SendContactMailUseCase };
