import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { SendContactMailUseCase } from './SendContactMailUseCase';

class SendContactMailController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, email, phone, message } = request.body;

    const sendContactMailUseCase = container.resolve(SendContactMailUseCase);

    await sendContactMailUseCase.execute({ name, email, phone, message });

    return response.json({
      message: 'Message sent',
    });
  }
}

export { SendContactMailController };
