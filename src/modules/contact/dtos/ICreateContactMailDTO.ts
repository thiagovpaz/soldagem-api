interface ICreateContactMailDTO {
  name: string;
  email: string;
  phone: string;
  message: string;
}

export { ICreateContactMailDTO };
