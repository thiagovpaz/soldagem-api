import { Router } from 'express';

import { SendContactMailController } from '@modules/contact/useCases/sendContactMail/SendContactMailController';

const contactRouter = Router();

const sendContactMailController = new SendContactMailController();

contactRouter.post('/', sendContactMailController.handle);

export { contactRouter };
