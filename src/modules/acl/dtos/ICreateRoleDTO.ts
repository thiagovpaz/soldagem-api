interface ICreateRoleDTO {
  name: string;
  description: string;
}

export { ICreateRoleDTO };
