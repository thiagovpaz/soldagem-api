interface ICreatePermissionDTO {
  name: string;
  description: string;
}

export { ICreatePermissionDTO };
