import { ICreatePermissionDTO } from '../dtos/ICreatePermissionDTO';
import { Permission } from '../infra/typeorm/entities/Permission';

interface IPermissionsRepository {
  create(data: ICreatePermissionDTO): Promise<Permission>;
  findByName(name: string): Promise<Permission | undefined>;
  findByIds(ids: string[]): Promise<Permission[] | undefined>;
  save(permission: Permission): Promise<Permission>;
  list(): Promise<Permission[]>;
}

export { IPermissionsRepository };
