import { ICreateRoleDTO } from '../dtos/ICreateRoleDTO';
import { Role } from '../infra/typeorm/entities/Role';

interface IRolesRepository {
  create(data: ICreateRoleDTO): Promise<Role>;
  findByName(name: string): Promise<Role | undefined>;
  findByIds(ids: string[]): Promise<Role[] | undefined>;
  save(role: Role): Promise<Role>;
  findById(id: string): Promise<Role | undefined>;
  list(): Promise<Role[]>;
}

export { IRolesRepository };
