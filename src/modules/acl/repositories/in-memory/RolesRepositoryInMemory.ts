import { ICreateRoleDTO } from '@modules/acl/dtos/ICreateRoleDTO';
import { IRolesRepository } from '../IRolesRepository';
import { Role } from '../../infra/typeorm/entities/Role';

class RolesRepositoryInMemory implements IRolesRepository {
  private roles: Role[] = [];

  async create({ name, description }: ICreateRoleDTO): Promise<Role> {
    const role = new Role();

    Object.assign(role, { name, description });

    this.roles.push(role);

    return role;
  }

  async findByName(name: string): Promise<Role | undefined> {
    return this.roles.find(role => role.name === name);
  }

  async findByIds(ids: string[]): Promise<Role[] | undefined> {
    return this.roles.filter(i => ids.includes(i.id));
  }

  async save(role: Role): Promise<Role> {
    const findIndex = this.roles.findIndex(r => r.id === role.id);

    this.roles[findIndex] = role;

    return role;
  }

  async findById(id: string): Promise<Role | undefined> {
    return this.roles.find(role => role.id === id);
  }

  async list(): Promise<Role[]> {
    return this.roles;
  }
}

export { RolesRepositoryInMemory };
