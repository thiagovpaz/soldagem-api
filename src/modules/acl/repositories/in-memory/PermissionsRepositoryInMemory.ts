import { ICreatePermissionDTO } from '@modules/acl/dtos/ICreatePermissionDTO';
import { IPermissionsRepository } from '../IPermissionsRepository';
import { Permission } from '../../infra/typeorm/entities/Permission';

class PermissionsRepositoryInMemory implements IPermissionsRepository {
  private permissions: Permission[] = [];

  async create({
    name,
    description,
  }: ICreatePermissionDTO): Promise<Permission> {
    const permission = new Permission();

    Object.assign(permission, { name, description });

    this.permissions.push(permission);

    return permission;
  }

  async findByName(name: string): Promise<Permission | undefined> {
    return this.permissions.find(permission => permission.name === name);
  }

  async findByIds(ids: string[]): Promise<Permission[] | undefined> {
    return this.permissions.filter(i => ids.includes(i.id));
  }

  async save(permission: Permission): Promise<Permission> {
    const findIndex = this.permissions.findIndex(p => p.id === permission.id);

    this.permissions[findIndex] = permission;

    return permission;
  }

  async list(): Promise<Permission[]> {
    return this.permissions;
  }
}

export { PermissionsRepositoryInMemory };
