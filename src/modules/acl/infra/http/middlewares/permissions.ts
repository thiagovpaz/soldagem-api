import { NextFunction, Request, Response } from 'express';

import { AppError } from '@shared/errors/AppError';
import { UsersRepository } from '@modules/users/infra/typeorm/repositories/UsersRepository';

export function can(permissionsRoutes: string[]) {
  return async (request: Request, response: Response, next: NextFunction) => {
    const { id: user_id } = request.user;

    const usersRepository = new UsersRepository();
    const user = await usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const permissionExists = (await user.permissions)
      .map(permission => permission.name)
      .some(permission => permissionsRoutes.includes(permission));

    if (!permissionExists) {
      throw new AppError('You cannot access this', 401);
    }

    return next();
  };
}

export function is(rolesRoutes: string[]) {
  return async (request: Request, response: Response, next: NextFunction) => {
    const { id: user_id } = request.user;

    const usersRepository = new UsersRepository();
    const user = await usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const roleExists = user.roles
      .map(role => role.name)
      .some(role => rolesRoutes.includes(role));

    if (!roleExists) {
      throw new AppError('You are not allowed to access', 403);
    }

    return next();
  };
}
