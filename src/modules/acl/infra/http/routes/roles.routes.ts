import { Router } from 'express';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import { CreateRoleController } from '@modules/acl/useCases/createRole/CreateRoleController';
import { CreateRolePermissionController } from '@modules/acl/useCases/createRolePermission/CreateRolePermissionController';
import { ListRolesController } from '@modules/acl/useCases/listRoles/ListRolesController';

const rolesRouter = Router();

const createRoleController = new CreateRoleController();
const listRolesController = new ListRolesController();
const createRolePermissionsController = new CreateRolePermissionController();

rolesRouter.use(ensureAuthenticated);

rolesRouter.post('/', createRoleController.handle);
rolesRouter.get('/', listRolesController.handle);
rolesRouter.post('/:role_id', createRolePermissionsController.handle);

export { rolesRouter };
