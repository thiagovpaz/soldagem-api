import { Router } from 'express';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import { CreatePermissionController } from '@modules/acl/useCases/createPermission/CreatePermissionController';
import { ListPermissionsController } from '@modules/acl/useCases/listPermissions/ListPermissionsController';

const permissionsRouter = Router();

const createPermissionController = new CreatePermissionController();
const listPermissionController = new ListPermissionsController();

permissionsRouter.use(ensureAuthenticated);

permissionsRouter.post('/', createPermissionController.handle);
permissionsRouter.get('/', listPermissionController.handle);

export { permissionsRouter };
