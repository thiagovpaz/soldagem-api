import { Router } from 'express';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import { CreateAccessControlListController } from '@modules/acl/useCases/createAccessControlList/CreateAccessControlListController';

const aclRouter = Router();

const createAccessControlListController =
  new CreateAccessControlListController();

aclRouter.post(
  '/',
  ensureAuthenticated,
  createAccessControlListController.handle,
);

export { aclRouter };
