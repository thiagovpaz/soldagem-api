import { getRepository, Repository } from 'typeorm';

import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { ICreateRoleDTO } from '@modules/acl/dtos/ICreateRoleDTO';
import { Role } from '../entities/Role';

class RolesRepository implements IRolesRepository {
  private repository: Repository<Role>;

  constructor() {
    this.repository = getRepository(Role);
  }

  async create({ name, description }: ICreateRoleDTO): Promise<Role> {
    const role = this.repository.create({ name, description });

    await this.repository.save(role);

    return role;
  }

  async findByName(name: string): Promise<Role | undefined> {
    return this.repository.findOne({ where: { name } });
  }

  async findByIds(ids: string[]): Promise<Role[] | undefined> {
    return this.repository.findByIds(ids);
  }

  async save(role: Role): Promise<Role> {
    return this.repository.save(role);
  }

  async findById(id: string): Promise<Role | undefined> {
    return this.repository.findOne(id);
  }

  async list(): Promise<Role[]> {
    return this.repository.find();
  }
}

export { RolesRepository };
