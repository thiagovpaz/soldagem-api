import { getRepository, Repository } from 'typeorm';

import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { ICreatePermissionDTO } from '@modules/acl/dtos/ICreatePermissionDTO';
import { Permission } from '../entities/Permission';

class PermissionsRepository implements IPermissionsRepository {
  private repository: Repository<Permission>;

  constructor() {
    this.repository = getRepository(Permission);
  }

  async create({
    name,
    description,
  }: ICreatePermissionDTO): Promise<Permission> {
    const permission = this.repository.create({ name, description });

    await this.repository.save(permission);

    return permission;
  }

  async findByName(name: string): Promise<Permission | undefined> {
    return this.repository.findOne({ where: { name } });
  }

  async findByIds(ids: string[]): Promise<Permission[] | undefined> {
    return this.repository.findByIds(ids);
  }

  async save(permission: Permission): Promise<Permission> {
    return this.repository.save(permission);
  }

  async list(): Promise<Permission[]> {
    return this.repository.find();
  }
}

export { PermissionsRepository };
