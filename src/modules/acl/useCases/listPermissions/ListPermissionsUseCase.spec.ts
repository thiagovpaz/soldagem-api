import { PermissionsRepositoryInMemory } from '@modules/acl/repositories/in-memory/PermissionsRepositoryInMemory';
import { ListPermissionsUseCase } from './ListPermissionsUseCase';

let permissionsRepositoryInMemory: PermissionsRepositoryInMemory;
let listPermissionsUseCase: ListPermissionsUseCase;

describe('ListPermissionsUseCase', () => {
  beforeEach(() => {
    permissionsRepositoryInMemory = new PermissionsRepositoryInMemory();

    listPermissionsUseCase = new ListPermissionsUseCase(
      permissionsRepositoryInMemory,
    );
  });

  it('should be able to list all permissions', async () => {
    await permissionsRepositoryInMemory.create({
      name: 'fake-permission',
      description: 'fake-permission-description',
    });

    const response = await listPermissionsUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'fake-permission',
        }),
      ]),
    );
  });
});
