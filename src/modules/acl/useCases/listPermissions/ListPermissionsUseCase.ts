import { inject, injectable } from 'tsyringe';

import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';

@injectable()
class ListPermissionsUseCase {
  constructor(
    @inject('PermissionsRepository')
    private permissionsRepository: IPermissionsRepository,
  ) {}

  async execute(): Promise<Permission[]> {
    const permissions = await this.permissionsRepository.list();

    return permissions;
  }
}

export { ListPermissionsUseCase };
