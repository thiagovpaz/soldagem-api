import { AppError } from '@shared/errors/AppError';
import { RolesRepositoryInMemory } from '@modules/acl/repositories/in-memory/RolesRepositoryInMemory';
import { PermissionsRepositoryInMemory } from '@modules/acl/repositories/in-memory/PermissionsRepositoryInMemory';
import { CreateRolePermissionUseCase } from './CreateRolePermissionUseCase';

let rolesRepositoryInMemory: RolesRepositoryInMemory;
let permissionsRepositoryInMemory: PermissionsRepositoryInMemory;
let createRolePermissionUseCase: CreateRolePermissionUseCase;

describe('CreateRolePermissionUseCase', () => {
  beforeEach(() => {
    rolesRepositoryInMemory = new RolesRepositoryInMemory();
    permissionsRepositoryInMemory = new PermissionsRepositoryInMemory();

    createRolePermissionUseCase = new CreateRolePermissionUseCase(
      rolesRepositoryInMemory,
      permissionsRepositoryInMemory,
    );
  });

  it('should be able to assign permissions to a role', async () => {
    const { id: role_id } = await rolesRepositoryInMemory.create({
      name: 'fake-role-name',
      description: 'fake-role-description',
    });

    const { id: permission_id } = await permissionsRepositoryInMemory.create({
      name: 'fake-permission-name',
      description: 'fake-permission-description',
    });

    const response = await createRolePermissionUseCase.execute({
      role_id,
      permissions: [permission_id],
    });

    expect(response.name).toBe('fake-role-name');
    expect(response.permissions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-permission-name' }),
      ]),
    );
  });

  it('should not be able to assign permissions to a invalid role', async () => {
    await expect(
      createRolePermissionUseCase.execute({
        role_id: 'fake-invalid-role-id',
        permissions: ['fake-permission-id'],
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
