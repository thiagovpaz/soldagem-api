import request from 'supertest';
import { Connection } from 'typeorm';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'admin',
  email: 'admin@admin.com',
  password: 'secret',
};

let connection: Connection;

describe('POST /roles/:id', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to assign permissions to a role', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const { body: role } = await request(app)
      .post('/v1/roles')
      .send({
        name: 'fake-role-name',
        description: 'fake-role-description',
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const { body: permission } = await request(app)
      .post('/v1/permissions')
      .send({
        name: 'fake-permission-name',
        description: 'fake-permission-description',
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const response = await request(app)
      .post(`/v1/roles/${role.id}`)
      .send({
        permissions: [permission.id],
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    expect(response.status).toEqual(201);
    expect(response.body.permissions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-permission-name' }),
      ]),
    );
  });
});
