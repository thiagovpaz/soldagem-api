import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';

interface IRequest {
  role_id: string;
  permissions: string[];
}

@injectable()
class CreateRolePermissionUseCase {
  constructor(
    @inject('RolesRepository')
    private rolesRepository: IRolesRepository,
    @inject('PermissionsRepository')
    private permissionsRepository: IPermissionsRepository,
  ) {}

  async execute({ role_id, permissions }: IRequest): Promise<Role> {
    const role = await this.rolesRepository.findById(role_id);
    if (!role) {
      throw new AppError('Role does not exists');
    }

    const permissionsExists = await this.permissionsRepository.findByIds(
      permissions,
    );

    Object.assign(role, { permissions: permissionsExists });

    await this.rolesRepository.save(role);

    return role;
  }
}

export { CreateRolePermissionUseCase };
