import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateRolePermissionUseCase } from './CreateRolePermissionUseCase';

class CreateRolePermissionController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { permissions } = request.body;
    const { role_id } = request.params;

    const createRolePermissionUseCase = container.resolve(
      CreateRolePermissionUseCase,
    );

    const role = await createRolePermissionUseCase.execute({
      role_id,
      permissions,
    });

    return response.status(201).json(role);
  }
}

export { CreateRolePermissionController };
