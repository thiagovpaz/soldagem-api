import { inject, injectable } from 'tsyringe';

import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';

@injectable()
class ListRolesUseCase {
  constructor(
    @inject('RolesRepository')
    private rolesRepository: IRolesRepository,
  ) {}
  async execute(): Promise<Role[]> {
    const roles = await this.rolesRepository.list();

    return roles;
  }
}

export { ListRolesUseCase };
