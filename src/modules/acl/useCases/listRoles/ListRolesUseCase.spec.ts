import { RolesRepositoryInMemory } from '@modules/acl/repositories/in-memory/RolesRepositoryInMemory';
import { ListRolesUseCase } from './ListRolesUseCase';

let rolesRepositoryInMemory: RolesRepositoryInMemory;
let listRolesUseCase: ListRolesUseCase;

describe('ListRolesUseCase', () => {
  beforeEach(() => {
    rolesRepositoryInMemory = new RolesRepositoryInMemory();

    listRolesUseCase = new ListRolesUseCase(rolesRepositoryInMemory);
  });

  it('should be able to list all roles', async () => {
    await rolesRepositoryInMemory.create({
      name: 'fake-role',
      description: 'fake-role-description',
    });

    const response = await listRolesUseCase.execute();

    expect(response).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'fake-role',
        }),
      ]),
    );
  });
});
