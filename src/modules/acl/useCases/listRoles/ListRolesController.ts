import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListRolesUseCase } from './ListRolesUseCase';

class ListRolesController {
  async handle(request: Request, response: Response): Promise<Response> {
    const listRolesUseCase = container.resolve(ListRolesUseCase);

    const permissions = await listRolesUseCase.execute();

    return response.json(permissions);
  }
}

export { ListRolesController };
