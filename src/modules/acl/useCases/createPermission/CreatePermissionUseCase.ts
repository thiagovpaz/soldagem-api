import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';

interface IRequest {
  name: string;
  description: string;
}

@injectable()
class CreatePermissionUseCase {
  constructor(
    @inject('PermissionsRepository')
    private permissionsRepository: IPermissionsRepository,
  ) {}

  async execute({ name, description }: IRequest): Promise<Permission> {
    const checkPermissionsExists = await this.permissionsRepository.findByName(
      name,
    );

    if (checkPermissionsExists) {
      throw new AppError('Permission already exists');
    }

    const permission = await this.permissionsRepository.create({
      name,
      description,
    });

    return permission;
  }
}

export { CreatePermissionUseCase };
