import { AppError } from '@shared/errors/AppError';
import { PermissionsRepositoryInMemory } from '@modules/acl/repositories/in-memory/PermissionsRepositoryInMemory';
import { CreatePermissionUseCase } from './CreatePermissionUseCase';

let permissionsRepositoryInMemory: PermissionsRepositoryInMemory;
let createPermissionUseCase: CreatePermissionUseCase;

describe('CreatePermissionUseCase', () => {
  beforeEach(() => {
    permissionsRepositoryInMemory = new PermissionsRepositoryInMemory();

    createPermissionUseCase = new CreatePermissionUseCase(
      permissionsRepositoryInMemory,
    );
  });

  it('should be able to create a permission', async () => {
    const response = await createPermissionUseCase.execute({
      name: 'fake-permission-name',
      description: 'fake-permission-description',
    });

    expect(response.name).toBe('fake-permission-name');
    expect(response.description).toBe('fake-permission-description');
  });

  it('should not be able to create a permission that already exists', async () => {
    await permissionsRepositoryInMemory.create({
      name: 'fake-permission-name',
      description: 'fake-permission-description',
    });

    await expect(
      createPermissionUseCase.execute({
        name: 'fake-permission-name',
        description: 'fake-permission-description',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
