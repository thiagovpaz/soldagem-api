import { AppError } from '@shared/errors/AppError';
import { PermissionsRepositoryInMemory } from '@modules/acl/repositories/in-memory/PermissionsRepositoryInMemory';
import { RolesRepositoryInMemory } from '@modules/acl/repositories/in-memory/RolesRepositoryInMemory';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { CreateAccessControlListUseCase } from './CreateAccessControlListUseCase';

let permissionsRepositoryInMemory: PermissionsRepositoryInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;
let rolesRepositoryInMemory: RolesRepositoryInMemory;
let createAccessControlListUseCase: CreateAccessControlListUseCase;

describe('CreateAccessControlListUseCase', () => {
  beforeEach(() => {
    permissionsRepositoryInMemory = new PermissionsRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();
    rolesRepositoryInMemory = new RolesRepositoryInMemory();

    createAccessControlListUseCase = new CreateAccessControlListUseCase(
      usersRepositoryInMemory,
      rolesRepositoryInMemory,
      permissionsRepositoryInMemory,
    );
  });

  it('should be able to create a acl', async () => {
    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const { id: permission_id } = await permissionsRepositoryInMemory.create({
      name: 'fake-permission-name',
      description: 'fake-permission-description',
    });

    const { id: role_id } = await rolesRepositoryInMemory.create({
      name: 'fake-role-name',
      description: 'fake-role-description',
    });

    const response = await createAccessControlListUseCase.execute({
      user_id,
      roles: [role_id],
      permissions: [permission_id],
    });

    expect(response).toHaveProperty('permissions');
    expect(response).toHaveProperty('roles');
    expect(response.roles).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-role-name' }),
      ]),
    );
    expect(response.permissions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-permission-name' }),
      ]),
    );
  });

  it('should not be able to create a acl', async () => {
    await expect(
      createAccessControlListUseCase.execute({
        user_id: 'fake-invalid-user-id',
        roles: ['fake-role-id'],
        permissions: ['fake-permission-id'],
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
