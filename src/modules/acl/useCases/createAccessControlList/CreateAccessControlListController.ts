import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreateAccessControlListUseCase } from './CreateAccessControlListUseCase';

class CreateAccessControlListController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { user_id, roles, permissions } = request.body;

    const createAccessControlListUseCase = container.resolve(
      CreateAccessControlListUseCase,
    );

    const acl = await createAccessControlListUseCase.execute({
      user_id,
      roles,
      permissions,
    });

    return response.status(201).json(acl);
  }
}

export { CreateAccessControlListController };
