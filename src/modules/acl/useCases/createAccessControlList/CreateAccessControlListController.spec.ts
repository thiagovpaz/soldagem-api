import request from 'supertest';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import createConnection from '@shared/infra/typeorm';
import { Connection } from 'typeorm';

const admin_user: ICreateUserDTO = {
  name: 'admin',
  email: 'admin@admin.com',
  password: 'secret',
};

const fake_user: ICreateUserDTO = {
  name: 'fake-name',
  email: 'fake@fake.com',
  password: 'fake-secret',
};

let connection: Connection;

describe('POST /acl', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a acl', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const { body: user } = await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const { body: role } = await request(app)
      .post('/v1/roles')
      .send({
        name: 'fake-role-name',
        description: 'fake-role-description',
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);
    const { body: permission } = await request(app)
      .post('/v1/permissions')
      .send({
        name: 'fake-permission-name',
        description: 'fake-permission-description',
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);
    const response = await request(app)
      .post('/v1/acl')
      .send({
        user_id: user.id,
        roles: [role.id],
        permissions: [permission.id],
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);
    expect(response.status).toEqual(201);
    expect(response.body.permissions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-permission-name' }),
      ]),
    );
    expect(response.body.roles).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ name: 'fake-role-name' }),
      ]),
    );
  });
});
