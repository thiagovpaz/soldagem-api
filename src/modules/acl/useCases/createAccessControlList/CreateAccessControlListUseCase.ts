import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { IPermissionsRepository } from '@modules/acl/repositories/IPermissionsRepository';
import { User } from '@modules/users/infra/typeorm/entities/User';

interface IRequest {
  user_id: string;
  roles: string[];
  permissions: string[];
}

@injectable()
class CreateAccessControlListUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
    @inject('RolesRepository')
    private rolesRepository: IRolesRepository,
    @inject('PermissionsRepository')
    private permissionsRepository: IPermissionsRepository,
  ) {}
  async execute({ user_id, roles, permissions }: IRequest): Promise<User> {
    const user = await this.usersRepository.findById(user_id);
    if (!user) {
      throw new AppError('User does not exists');
    }

    const permissionsExists = await this.permissionsRepository.findByIds(
      permissions,
    );

    const rolesExists = await this.rolesRepository.findByIds(roles);

    Object.assign(user, { permissions: permissionsExists, roles: rolesExists });

    await this.usersRepository.save(user);

    return user;
  }
}

export { CreateAccessControlListUseCase };
