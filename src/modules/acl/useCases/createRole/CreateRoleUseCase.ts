import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IRolesRepository } from '@modules/acl/repositories/IRolesRepository';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';

interface IRequest {
  name: string;
  description: string;
}

@injectable()
class CreateRoleUseCase {
  constructor(
    @inject('RolesRepository')
    private rolesRepository: IRolesRepository,
  ) {}

  async execute({ name, description }: IRequest): Promise<Role> {
    const checkRolesExists = await this.rolesRepository.findByName(name);

    if (checkRolesExists) {
      throw new AppError('Role already exists');
    }

    const role = await this.rolesRepository.create({
      name,
      description,
    });

    return role;
  }
}

export { CreateRoleUseCase };
