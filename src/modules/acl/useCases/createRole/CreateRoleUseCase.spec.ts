import { AppError } from '@shared/errors/AppError';
import { RolesRepositoryInMemory } from '@modules/acl/repositories/in-memory/RolesRepositoryInMemory';
import { CreateRoleUseCase } from './CreateRoleUseCase';

let rolesRepositoryInMemory: RolesRepositoryInMemory;
let createRoleUseCase: CreateRoleUseCase;

describe('CreateRoleUseCase', () => {
  beforeEach(() => {
    rolesRepositoryInMemory = new RolesRepositoryInMemory();

    createRoleUseCase = new CreateRoleUseCase(rolesRepositoryInMemory);
  });

  it('should be able to create a role', async () => {
    const response = await createRoleUseCase.execute({
      name: 'fake-role',
      description: 'fake-role-description',
    });

    expect(response.name).toBe('fake-role');
    expect(response.description).toBe('fake-role-description');
  });

  it('should not be able to create a role that already exists', async () => {
    await rolesRepositoryInMemory.create({
      name: 'fake-role',
      description: 'fake-role-description',
    });

    await expect(
      createRoleUseCase.execute({
        name: 'fake-role',
        description: 'fake-role-description',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
