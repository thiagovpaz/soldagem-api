import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { ISearchParamsDTO } from '@modules/users/dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '@modules/users/dtos/ISearchResponseDTO';
import { User } from '@modules/users/infra/typeorm/entities/User';
import { IUsersRepository } from '../IUsersRepository';

class UsersRepositoryInMemory implements IUsersRepository {
  private users: User[] = [];

  async create(data: ICreateUserDTO): Promise<User> {
    const user = new User();

    Object.assign(user, data);

    this.users.push(user);

    return user;
  }

  async findById(id: string, _relations?: string[]): Promise<User | undefined> {
    return this.users.find(user => user.id === id);
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.users.find(user => user.email === email);
  }

  async findByAvatar(avatar: string): Promise<User | undefined> {
    return this.users.find(user => user.avatar === avatar);
  }

  async save(user: User): Promise<User> {
    const findIndex = this.users.findIndex(u => u.id === user.id);

    this.users[findIndex] = user;

    return user;
  }

  async list(_params: ISearchParamsDTO): Promise<ISearchResponseDTO> {
    return { users: this.users, total: this.users.length };
  }

  async delete(user_id: string): Promise<void> {
    this.users = this.users.filter(u => u.id !== user_id);
  }
}

export { UsersRepositoryInMemory };
