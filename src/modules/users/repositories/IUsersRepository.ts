import { ICreateUserDTO } from '../dtos/ICreateUserDTO';
import { ISearchParamsDTO } from '../dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '../dtos/ISearchResponseDTO';
import { User } from '../infra/typeorm/entities/User';

interface IUsersRepository {
  create(data: ICreateUserDTO): Promise<User>;
  findById(id: string, relations?: string[]): Promise<User | undefined>;
  findByEmail(email: string): Promise<User | undefined>;
  findByAvatar(avatar_filename: string): Promise<User | undefined>;
  save(user: User): Promise<User>;
  list(params: ISearchParamsDTO): Promise<ISearchResponseDTO>;
  delete(user_id: string): Promise<void>;
}

export { IUsersRepository };
