import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { UsersTokensRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersTokensRepositoryInMemory';
import { DayjsDateProvider } from '@shared/container/providers/DateProvider/implementations/DayjsDateProvider';
import { ResetPasswordUserUseCase } from './ResetPasswordUserUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let usersTokensRepositoryInMemory: UsersTokensRepositoryInMemory;
let dayjsDateProvider: DayjsDateProvider;
let resetPasswordUserUseCase: ResetPasswordUserUseCase;

describe('ResetPasswordUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();
    usersTokensRepositoryInMemory = new UsersTokensRepositoryInMemory();
    dayjsDateProvider = new DayjsDateProvider();

    resetPasswordUserUseCase = new ResetPasswordUserUseCase(
      usersRepositoryInMemory,
      usersTokensRepositoryInMemory,
      dayjsDateProvider,
    );
  });

  it('should be able to reset password', async () => {
    const deleteById = jest.spyOn(usersTokensRepositoryInMemory, 'deleteById');

    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const { refresh_token: token } = await usersTokensRepositoryInMemory.create(
      {
        user_id,
        refresh_token: 'fake-refresh-token',
        expires_date: new Date('2030-12-31T00:00:00.000Z'),
      },
    );

    await resetPasswordUserUseCase.execute({
      token,
      password: 'fake-password',
    });

    expect(deleteById).toBeCalled();
  });

  it('should not be able to reset password with a expired token', async () => {
    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const { refresh_token: token } = await usersTokensRepositoryInMemory.create(
      {
        user_id,
        refresh_token: 'fake-refresh-token',
        expires_date: new Date('2001-12-31T00:00:00.000Z'),
      },
    );

    await expect(
      resetPasswordUserUseCase.execute({
        token,
        password: 'fake-password',
      }),
    ).rejects.toEqual(new AppError('Token expired'));
  });

  it('should not be able to reset password with a invalid token', async () => {
    await expect(
      resetPasswordUserUseCase.execute({
        token: 'fake-invalid-token',
        password: 'fake-password',
      }),
    ).rejects.toEqual(new AppError('Invalid token'));
  });
});
