import request from 'supertest';
import { Connection } from 'typeorm';
import { useSeeding, factory } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import createConnection from '@shared/infra/typeorm';
import { UserToken } from '@modules/users/infra/typeorm/entities/UserToken';
import { User } from '@modules/users/infra/typeorm/entities/User';

let connection: Connection;
let token: string;

describe('POST /passwords/reset', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });

    const user = await factory(User)().create();
    token = (await factory(UserToken)().create({ user_id: user.id }))
      .refresh_token;
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to change a password', async () => {
    const response = await request(app)
      .post('/v1/passwords/reset')
      .send({ token, password: 'fake-password' })
      .set('Accept', 'application/json');

    expect(response.status).toEqual(204);
  });
});
