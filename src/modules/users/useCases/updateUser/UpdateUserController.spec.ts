import request from 'supertest';
import { Connection } from 'typeorm';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import { CreateUsers } from '@shared/infra/typeorm/seeds/user.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'secret',
};

let connection: Connection;

describe('PUT /users/:user_id', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
    await runSeeder(CreateUsers);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to update a user by id', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const response = await request(app)
      .put('/v1/users/8c906152-4a46-46e0-b852-163e24928293')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .send({
        name: 'User Updated',
        email: 'other@user.com',
      })
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(200);
    expect(response.body.name).toEqual('User Updated');
    expect(response.body.email).toEqual('other@user.com');
  });
});
