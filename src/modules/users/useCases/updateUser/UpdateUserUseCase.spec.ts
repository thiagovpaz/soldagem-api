import { Client } from '@modules/users/infra/typeorm/entities/Client';
import { UserProfile } from '@modules/users/infra/typeorm/entities/UserProfile';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';
import { UpdateUserUseCase } from './UpdateUserUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let updateUserUseCase: UpdateUserUseCase;

describe('UpdateUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    updateUserUseCase = new UpdateUserUseCase(usersRepositoryInMemory);
  });

  it('should be able to update a user', async () => {
    const user = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await updateUserUseCase.execute({
      user_id: user.id,
      data: {
        name: 'fake-name-updated',
        password: 'fake-password-updated',
        profile: {
          company: 'fake-company',
        } as UserProfile,
      },
    });

    expect(response).toHaveProperty('id');
    expect(response.name).toBe('fake-name-updated');
  });

  it('should be able to update a client', async () => {
    const user = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await updateUserUseCase.execute({
      user_id: user.id,
      data: {
        name: 'fake-name-updated',
        password: 'fake-password-updated',
        client: {
          cnpj: 'fake-cnpj',
        } as Client,
      },
    });

    expect(response).toHaveProperty('id');
    expect(response.name).toBe('fake-name-updated');
  });

  it('should not be able to update a user with an invalid user', async () => {
    await expect(
      updateUserUseCase.execute({
        user_id: 'fake-invalid-user-id',
      }),
    ).rejects.toEqual(new AppError('User does not exists'));
  });
});
