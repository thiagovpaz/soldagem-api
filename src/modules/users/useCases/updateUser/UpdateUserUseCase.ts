import { inject, injectable } from 'tsyringe';
import { hash } from 'bcrypt';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { User } from '@modules/users/infra/typeorm/entities/User';
import { UserMap } from '@modules/users/mapper/UserMap';
import { IUserResponseDTO } from '@modules/users/dtos/IUserResponseDTO';

interface IRequest {
  user_id: string;
  data?: Partial<User>;
}

@injectable()
class UpdateUserUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute({ user_id, data }: IRequest): Promise<IUserResponseDTO> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const { name, email, avatar, password, status } = data;

    const user_data = { name, email, avatar, status };

    if (password) {
      const hashedPassword = await hash(password, 8);
      user.password = hashedPassword;
    }

    if (data.client) {
      user.client = { ...user.client, ...data.client };
    }

    if (data.profile) {
      user.profile = { ...user.profile, ...data.profile };
    }

    Object.assign(user, user_data);

    await this.usersRepository.save(user);

    return UserMap.toDTO(user);
  }
}

export { UpdateUserUseCase };
