import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { ListUsersUseCase } from './ListUsersUseCase';

let listUsersUseCase: ListUsersUseCase;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe('ListUsersUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    listUsersUseCase = new ListUsersUseCase(usersRepositoryInMemory);
  });

  it('should be able to list all users', async () => {
    await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await listUsersUseCase.execute({});

    expect(response).toEqual(
      expect.objectContaining({
        users: expect.arrayContaining([
          expect.objectContaining({
            name: 'fake-name',
          }),
        ]),
      }),
    );
  });
});
