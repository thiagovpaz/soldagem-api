import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { instanceToPlain } from 'class-transformer';

import { ISearchParamsDTO } from '@modules/users/dtos/ISearchParamsDTO';
import { ListUsersUseCase } from './ListUsersUseCase';

class ListUsersController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { filter, query, page, per_page } = request.query as ISearchParamsDTO;

    const listUsersUseCase = container.resolve(ListUsersUseCase);

    const { users, total } = await listUsersUseCase.execute({
      filter: filter || null,
      query: query || null,
      page: page || 1,
      per_page: per_page || 10,
    });

    return response.json({ total, users: instanceToPlain(users) });
  }
}

export { ListUsersController };
