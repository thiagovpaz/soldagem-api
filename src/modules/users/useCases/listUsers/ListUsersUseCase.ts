import { injectable, inject } from 'tsyringe';

import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { User } from '@modules/users/infra/typeorm/entities/User';
import { ISearchParamsDTO } from '@modules/users/dtos/ISearchParamsDTO';

interface IResponse {
  users: User[];
  total: number;
}

@injectable()
class ListUsersUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute({
    filter,
    query,
    page,
    per_page,
  }: ISearchParamsDTO): Promise<IResponse> {
    const { users, total } = await this.usersRepository.list({
      filter,
      query,
      page,
      per_page,
    });

    return { total, users };
  }
}

export { ListUsersUseCase };
