import request from 'supertest';
import { Connection } from 'typeorm';
import { useSeeding, runSeeder } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'admin',
  email: 'admin@admin.com',
  password: 'secret',
};

let connection: Connection;

jest.setTimeout(30000);

describe('POST /passwords/forgot', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to send a forgot password mail to user', async () => {
    const response = await request(app)
      .post('/v1/passwords/forgot')
      .send({ email: admin_user.email })
      .set('Accept', 'application/json');

    expect(response.status).toEqual(204);
  });
});
