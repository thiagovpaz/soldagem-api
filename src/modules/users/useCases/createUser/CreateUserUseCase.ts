import { inject, injectable } from 'tsyringe';
import { hash } from 'bcrypt';

import { AppError } from '@shared/errors/AppError';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { UserMap } from '@modules/users/mapper/UserMap';
import { IUserResponseDTO } from '@modules/users/dtos/IUserResponseDTO';

@injectable()
class CreateUserUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute(data: ICreateUserDTO): Promise<IUserResponseDTO> {
    const checkUserExists = await this.usersRepository.findByEmail(data.email);

    if (checkUserExists) {
      throw new AppError('User already exists');
    }

    const hashedPassword = await hash(data.password, 8);
    const user_data = { ...data, password: hashedPassword };

    const user = await this.usersRepository.create(user_data);

    return UserMap.toDTO(user);
  }
}

export { CreateUserUseCase };
