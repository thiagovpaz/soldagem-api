import request from 'supertest';
import { Connection } from 'typeorm';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'secret',
};

const fake_user: ICreateUserDTO = {
  name: 'fake-name',
  email: 'fake@fake.com',
  password: 'fake-secret',
};

let connection: Connection;

describe('POST /users', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a user', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const response = await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(201);
    expect(response.body.email).toEqual(fake_user.email);
  });

  it('should not be able create user with an existing email', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const response = await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(400);
  });
});
