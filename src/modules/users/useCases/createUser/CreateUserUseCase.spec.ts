import { AppError } from '@shared/errors/AppError';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { CreateUserUseCase } from './CreateUserUseCase';

let createUserUseCase: CreateUserUseCase;
let usersRepositoryInMemory: UsersRepositoryInMemory;

const fake_user: ICreateUserDTO = {
  name: 'fake-name',
  email: 'fake@fake.com',
  password: 'fake-secret',
  client: {
    cnpj: 'fake-cnpj',
  },
  profile: {
    company: 'fake-company',
  },
};

describe('CreateUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
  });

  it('should be able to create a user', async () => {
    const user = await createUserUseCase.execute(fake_user);

    expect(user).toHaveProperty('id');
    expect(user.name).toEqual(fake_user.name);
  });

  it('should not be able to create a user with an existing email', async () => {
    await createUserUseCase.execute(fake_user);

    await expect(createUserUseCase.execute(fake_user)).rejects.toBeInstanceOf(
      AppError,
    );
  });
});
