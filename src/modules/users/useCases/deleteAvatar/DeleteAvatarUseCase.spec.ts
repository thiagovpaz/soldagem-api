import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { StorageProviderInMemory } from '@shared/container/providers/StorageProvider/in-memory/StorageProviderInMemory';
import { DeleteAvatarUseCase } from './DeleteAvatarUseCase';

let deleteAvatarUseCase: DeleteAvatarUseCase;
let storageProviderInMemory: StorageProviderInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe('DeleteAvatarUseCase', () => {
  beforeEach(() => {
    storageProviderInMemory = new StorageProviderInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    deleteAvatarUseCase = new DeleteAvatarUseCase(
      usersRepositoryInMemory,
      storageProviderInMemory,
    );
  });

  it('should be able to delete avatar', async () => {
    const spyDelete = jest.spyOn(storageProviderInMemory, 'delete');

    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    await deleteAvatarUseCase.execute(user_id);

    expect(spyDelete).toBeCalled();
  });

  it('should be able to delete avatar if user does not exists', async () => {
    const user_id = 'fake-invalid-user-id';

    await expect(deleteAvatarUseCase.execute(user_id)).rejects.toEqual(
      new AppError('User does not exists'),
    );
  });
});
