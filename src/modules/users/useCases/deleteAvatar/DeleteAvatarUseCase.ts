import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { IStorageProvider } from '@shared/container/providers/StorageProvider/IStorageProvider';

@injectable()
class DeleteAvatarUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
    @inject('StorageProvider')
    private storageProvider: IStorageProvider,
  ) {}
  async execute(avatar_filename: string): Promise<void> {
    const user = await this.usersRepository.findByAvatar(avatar_filename);

    if (!user) {
      await this.storageProvider.delete(avatar_filename, 'avatars');

      throw new AppError('User does not exists');
    }

    if (user.avatar) {
      await this.storageProvider.delete(user.avatar, 'avatars');

      user.avatar = null;

      await this.usersRepository.save(user);
    }

    // await this.storageProvider.delete(avatar_filename, 'avatars');
  }
}

export { DeleteAvatarUseCase };
