import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { DeleteAvatarUseCase } from './DeleteAvatarUseCase';

class DeleteAvatarController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { avatar_filename } = request.body;

    const deleteAvatarUseCase = container.resolve(DeleteAvatarUseCase);

    await deleteAvatarUseCase.execute(avatar_filename);

    return response.status(204).json();
  }
}

export { DeleteAvatarController };
