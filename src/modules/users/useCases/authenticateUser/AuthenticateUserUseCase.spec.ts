import { AppError } from '@shared/errors/AppError';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { CreateUserUseCase } from '../createUser/CreateUserUseCase';
import { AuthenticateUserUseCase } from './AuthenticateUserUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let createUserUseCase: CreateUserUseCase;
let authenticateUserUseCase: AuthenticateUserUseCase;

const fake_user: ICreateUserDTO = {
  name: 'fake-name',
  email: 'fake@fake.com',
  password: 'fake-secret',
};

describe('AuthenticateUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    authenticateUserUseCase = new AuthenticateUserUseCase(
      usersRepositoryInMemory,
    );
  });

  it('should be able to create a token with valid credentials', async () => {
    await createUserUseCase.execute(fake_user);

    const response = await authenticateUserUseCase.execute({
      email: fake_user.email,
      password: fake_user.password,
    });

    expect(response).toHaveProperty('token');
    expect(response).toHaveProperty('user');
    expect(response.user).not.toHaveProperty('password');
  });

  it('should not be able to authenticate with invalid credentials', async () => {
    await createUserUseCase.execute(fake_user);

    await expect(
      authenticateUserUseCase.execute({
        email: fake_user.email,
        password: 'fake-invalid-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to authenticate with invalid user', async () => {
    await expect(
      authenticateUserUseCase.execute({
        email: 'fake-invalid-email',
        password: 'fake-invalid-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
