import request from 'supertest';
import { Connection } from 'typeorm';
import { useSeeding, runSeeder } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'admin',
  email: 'admin@admin.com',
  password: 'secret',
};

const fake_user: ICreateUserDTO = {
  name: 'fake-name',
  email: 'fake-email@email.com',
  password: 'fake-secret',
};

let connection: Connection;

describe('POST /auth', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a token with valid credentials', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({ email: admin_user.email, password: admin_user.password })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const response = await request(app)
      .post('/v1/auth')
      .send({ email: fake_user.email, password: fake_user.password })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(201);
    expect(response.body).toHaveProperty('token');
    expect(response.body.user.email).toEqual(fake_user.email);
    expect(response.body.user).not.toHaveProperty('password');
  });

  it('should not be able to authenticate with invalid credentials', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    await request(app)
      .post('/v1/users')
      .send(fake_user)
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    const response = await request(app)
      .post('/v1/auth')
      .send({
        email: fake_user.email,
        password: 'fake-invalid-password',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(400);
    expect(response.body).not.toHaveProperty('token');
  });

  it('should not be able to authenticate with invalid user', async () => {
    const response = await request(app)
      .post('/v1/auth')
      .send({
        email: 'fake-invalid-email',
        password: 'fake-invalid-password',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    expect(response.headers['content-type']).toMatch(/json/);
    expect(response.status).toEqual(400);
    expect(response.body).not.toHaveProperty('token');
  });
});
