import request from 'supertest';
import { Connection } from 'typeorm';
import { useSeeding, runSeeder } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import createConnection from '@shared/infra/typeorm';

let connection: Connection;

describe('PUT /me', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to update a user profile', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: 'admin@admin.com',
        password: 'secret',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const response = await request(app)
      .put('/v1/me')
      .send({
        name: 'fake-new-name',
        email: 'fake-admin@admin.com',
      })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    expect(response.status).toEqual(200);
    expect(response.body.name).toEqual('fake-new-name');
    expect(response.body.email).toEqual('fake-admin@admin.com');
  });
});
