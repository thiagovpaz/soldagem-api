import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { UpdateProfileUseCase } from './UpdateProfileUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let updateProfileUseCase: UpdateProfileUseCase;

describe('UpdateProfileUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    updateProfileUseCase = new UpdateProfileUseCase(usersRepositoryInMemory);
  });

  it('should be able to update a user profile', async () => {
    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await updateProfileUseCase.execute({
      user_id,
      name: 'new-fake-name',
      email: 'new-fake@fake.com',
      password: 'new-fake-password',
      client: {
        cnpj: 'fake-cnpj',
      },
      profile: {
        company: 'fake-company',
      },
    });

    expect(response.name).toBe('new-fake-name');
    expect(response.email).toBe('new-fake@fake.com');
  });

  it('should not be able to update a user profile with an invalid user', async () => {
    await expect(
      updateProfileUseCase.execute({
        user_id: 'fake-invalid-user-id',
        name: 'new-fake-name',
        email: 'new-fake@fake.com',
      }),
    ).rejects.toEqual(new AppError('User does not exists'));
  });
});
