import { inject, injectable } from 'tsyringe';
import { hash } from 'bcrypt';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { IUserResponseDTO } from '@modules/users/dtos/IUserResponseDTO';
import { UserMap } from '@modules/users/mapper/UserMap';
import { ICreateUserClientDTO } from '@modules/users/dtos/ICreateClientDTO';
import { ICreateProfileDTO } from '@modules/users/dtos/ICreateProfileDTO';

interface IRequest {
  user_id: string;
  name?: string;
  email?: string;
  password?: string;
  client?: ICreateUserClientDTO;
  profile?: ICreateProfileDTO;
}

@injectable()
class UpdateProfileUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute({
    user_id,
    name,
    email,
    password,
    client,
    profile,
  }: IRequest): Promise<IUserResponseDTO> {
    const findUser = await this.usersRepository.findById(user_id);

    if (!findUser) {
      throw new AppError('User does not exists');
    }

    if (password) {
      const hashedPassword = await hash(password, 8);
      findUser.password = hashedPassword;
    }

    if (client) {
      findUser.client = { ...findUser.client, ...client };
    }

    if (profile) {
      findUser.profile = { ...findUser.profile, ...profile };
    }

    Object.assign(findUser, { name, email });

    await this.usersRepository.save(findUser);

    return UserMap.toDTO(findUser);
  }
}

export { UpdateProfileUseCase };
