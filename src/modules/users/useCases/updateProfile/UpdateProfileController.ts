import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UpdateProfileUseCase } from './UpdateProfileUseCase';

class UpdateProfileController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, email, password, client, profile } = request.body;
    const { id: user_id } = request.user;

    const updateProfileUseCase = container.resolve(UpdateProfileUseCase);

    const getProfile = await updateProfileUseCase.execute({
      user_id,
      name,
      email,
      password,
      client,
      profile,
    });

    return response.json(getProfile);
  }
}

export { UpdateProfileController };
