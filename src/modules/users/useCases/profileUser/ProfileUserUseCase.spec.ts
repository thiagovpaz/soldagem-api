import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { ProfileUserUseCase } from './ProfileUserUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let profileUserUseCase: ProfileUserUseCase;

describe('ProfileUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    profileUserUseCase = new ProfileUserUseCase(usersRepositoryInMemory);
  });

  it('should be able to retrieve a user profile', async () => {
    const { id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await profileUserUseCase.execute(id);

    expect(response.name).toBe('fake-name');
    expect(response.email).toBe('fake@fake.com');
  });

  it('should not be able to retrieve a user profile with an invalid user', async () => {
    await expect(
      profileUserUseCase.execute('fake-invalid-user-id'),
    ).rejects.toEqual(new AppError('User does not exists'));
  });
});
