import { AppError } from '@shared/errors/AppError';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { DeleteUserUseCase } from './DeleteUserUseCase';

let deleteUserUseCase: DeleteUserUseCase;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe('DeleteUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    deleteUserUseCase = new DeleteUserUseCase(usersRepositoryInMemory);
  });

  it('should be able to delete a user', async () => {
    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const deletUser = jest.spyOn(usersRepositoryInMemory, 'delete');

    await deleteUserUseCase.execute([user_id]);

    expect(deletUser).toHaveBeenCalledWith(user_id);
  });

  it('should not be able to delete an invalid user', async () => {
    await expect(
      deleteUserUseCase.execute(['fake-invalid-user-id']),
    ).rejects.toEqual(new AppError('User does not exists'));
  });
});
