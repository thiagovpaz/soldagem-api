import { injectable, inject } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';

@injectable()
class DeleteUserUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}
  async execute(users_ids: string[]): Promise<void> {
    const removeAll = users_ids.map(async user_id => {
      const user = await this.usersRepository.findById(user_id);

      if (!user) {
        throw new AppError('User does not exists');
      }

      await this.usersRepository.delete(user_id);
    });

    await Promise.all(removeAll);
  }
}

export { DeleteUserUseCase };
