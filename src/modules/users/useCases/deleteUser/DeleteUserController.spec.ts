import request from 'supertest';
import { Connection } from 'typeorm';
import { runSeeder, useSeeding } from 'typeorm-seeding';

import { app } from '@shared/infra/http/app';
import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import { CreateUsers } from '@shared/infra/typeorm/seeds/user.seed';
import createConnection from '@shared/infra/typeorm';

const admin_user: ICreateUserDTO = {
  name: 'Admin',
  email: 'admin@admin.com',
  password: 'secret',
};

let connection: Connection;

describe('DELETE /users', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
    await runSeeder(CreateUsers);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to delete a user by id', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: admin_user.email,
        password: admin_user.password,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const response = await request(app)
      .delete('/v1/users')
      .send({ users_ids: ['8c906152-4a46-46e0-b852-163e24928293'] })
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`);

    expect(response.status).toEqual(204);
  });
});
