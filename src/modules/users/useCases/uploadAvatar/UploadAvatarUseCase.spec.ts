import { AppError } from '@shared/errors/AppError';
import { StorageProviderInMemory } from '@shared/container/providers/StorageProvider/in-memory/StorageProviderInMemory';
import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { UploadAvatarUseCase } from './UploadAvatarUseCase';

let uploadAvatarUseCase: UploadAvatarUseCase;
let storageProviderInMemory: StorageProviderInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe('UploadAvatarUseCase', () => {
  beforeEach(() => {
    storageProviderInMemory = new StorageProviderInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    uploadAvatarUseCase = new UploadAvatarUseCase(
      storageProviderInMemory,
      usersRepositoryInMemory,
    );
  });

  it('should be able to upload an avatar', async () => {
    const { id: user_id } = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await uploadAvatarUseCase.execute({
      filename: 'fake-avatar.jpeg',
      user_id,
    });

    expect(response).toBe('fake-avatar.jpeg');
  });

  it('should not be able to upload an avatar with invalid user', async () => {
    await expect(
      uploadAvatarUseCase.execute({
        filename: 'fake-avatar.jpeg',
        user_id: 'fake-invalid-user-id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
