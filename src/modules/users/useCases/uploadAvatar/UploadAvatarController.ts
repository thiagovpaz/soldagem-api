import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UploadAvatarUseCase } from './UploadAvatarUseCase';

class UploadAvatarController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { filename } = request.file;
    const { id: user_id } = request.user;
    const { owner } = request.body;

    const uploadAvatarUseCase = container.resolve(UploadAvatarUseCase);

    const file = await uploadAvatarUseCase.execute({
      filename,
      user_id,
      owner,
    });

    return response.status(201).json({ filename: file });
  }
}

export { UploadAvatarController };
