import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IStorageProvider } from '@shared/container/providers/StorageProvider/IStorageProvider';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';

interface IRequest {
  filename: string;
  user_id: string;
  owner?: boolean;
}

@injectable()
class UploadAvatarUseCase {
  constructor(
    @inject('StorageProvider')
    private storageProvider: IStorageProvider,
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute({
    filename,
    user_id,
    owner = false,
  }: IRequest): Promise<string> {
    const findUser = await this.usersRepository.findById(user_id);

    if (!findUser) {
      throw new AppError('User does not exists');
    }

    const image_name = await this.storageProvider.save(filename, 'avatars');

    if (owner) {
      findUser.avatar = image_name;

      await this.usersRepository.save(findUser);
    }

    return image_name;
  }
}

export { UploadAvatarUseCase };
