import request from 'supertest';
import { Connection } from 'typeorm';
import { useSeeding, runSeeder } from 'typeorm-seeding';
import { resolve } from 'path';
import fs from 'fs/promises';

import upload from '@config/upload';
import { app } from '@shared/infra/http/app';
import { CreateAdmin } from '@shared/infra/typeorm/seeds/admin.seed';
import createConnection from '@shared/infra/typeorm';

let connection: Connection;

async function removeFile(fileName: string) {
  await fs.unlink(resolve(`${upload.tmpFolder}/avatars/`, fileName));
}

describe('POST /users/avatar', () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    await useSeeding({ connection: 'test' });
    await runSeeder(CreateAdmin);
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to upload an avatar', async () => {
    const { body } = await request(app)
      .post('/v1/auth')
      .send({
        email: 'admin@admin.com',
        password: 'secret',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);

    const buffer = Buffer.from('fake-avatar-data');

    const response = await request(app)
      .post('/v1/users/avatar')
      .attach('avatar', buffer, 'avatar.jpeg')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${body.token}`)
      .expect('Content-Type', /json/);

    await removeFile(response.body.filename);

    expect(response.status).toEqual(201);
    expect(response.body).toHaveProperty('filename');
  });
});
