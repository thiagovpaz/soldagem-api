import { UsersRepositoryInMemory } from '@modules/users/repositories/in-memory/UsersRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';
import { GetUserUseCase } from './GetUserUseCase';

let usersRepositoryInMemory: UsersRepositoryInMemory;
let getUserUseCase: GetUserUseCase;

describe('GetUserUseCase', () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    getUserUseCase = new GetUserUseCase(usersRepositoryInMemory);
  });

  it('should be able to get a user by id', async () => {
    const user = await usersRepositoryInMemory.create({
      name: 'fake-name',
      email: 'fake@fake.com',
      password: 'fake-password',
    });

    const response = await getUserUseCase.execute(user.id);

    expect(response).toHaveProperty('id');
    expect(response.id).toBe(user.id);
  });

  it('should not be able to get a user with an invalid id', async () => {
    await expect(
      getUserUseCase.execute('fake-invalid-user-id'),
    ).rejects.toEqual(new AppError('User does not exists'));
  });
});
