import { inject, injectable } from 'tsyringe';

import { AppError } from '@shared/errors/AppError';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { UserMap } from '@modules/users/mapper/UserMap';
import { IUserResponseDTO } from '@modules/users/dtos/IUserResponseDTO';

@injectable()
class GetUserUseCase {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  async execute(user_id: string): Promise<IUserResponseDTO> {
    const findUser = await this.usersRepository.findById(user_id);

    if (!findUser) {
      throw new AppError('User does not exists');
    }

    return UserMap.toDTO(findUser);
  }
}

export { GetUserUseCase };
