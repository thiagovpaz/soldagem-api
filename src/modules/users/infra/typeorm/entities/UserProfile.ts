import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './BaseEntity';
import { User } from './User';

@Entity('profiles')
class UserProfile extends BaseEntity {
  @Column()
  company: string;

  @Column()
  register: string;

  @Column()
  cpf: string;

  @Column()
  birthdate: Date;

  @Column()
  phone: string;

  @Column()
  qualification: string;

  @Column()
  position: string;

  @Column()
  certifying_entity: string;

  @Column()
  certification_validity: Date;

  @OneToOne(() => User, user => user.profile, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;
}

export { UserProfile };
