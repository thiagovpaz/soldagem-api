import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './BaseEntity';
import { User } from './User';

@Entity('clients')
class Client extends BaseEntity {
  @Column()
  cnpj: string;

  @OneToOne(() => User, user => user.client, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;
}

export { Client };
