import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

import uploadConfig from '@config/upload';

import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';
import { Welder } from '@modules/welders/infra/typeorm/entities/Welder';
import { BaseEntity } from './BaseEntity';
import { Client } from './Client';
import { UserProfile } from './UserProfile';

@Entity('users')
class User extends BaseEntity {
  @Column()
  name: string;

  @Column()
  email: string;

  @Exclude()
  @Column()
  password: string;

  @Column({
    nullable: true,
  })
  avatar: string;

  @Expose({ name: 'avatar_url' })
  avatar_url(): string | null {
    if (!this.avatar) {
      return null;
    }

    switch (uploadConfig.driver) {
      case 'local':
        return `${process.env.APP_API_URL}avatars/${this.avatar}`;
      case 's3':
        return `https://${uploadConfig.config.aws.bucket}.s3.amazonaws.com/${this.avatar}`;
      default:
        return null;
    }
  }

  @Column({
    nullable: true,
  })
  status: boolean;

  @ManyToMany(() => Role, { eager: true })
  @JoinTable({
    name: 'users_roles',
    joinColumns: [{ name: 'user_id' }],
    inverseJoinColumns: [{ name: 'role_id' }],
  })
  roles: Role[];

  @ManyToMany(() => Permission, { eager: true, cascade: true })
  @JoinTable({
    name: 'users_permissions',
    joinColumns: [{ name: 'user_id' }],
    inverseJoinColumns: [{ name: 'permission_id' }],
  })
  permissions: Permission[];

  @Expose()
  permissions_grouped() {
    if (this.permissions.length) {
      return this.permissions
        .map(p => p.description.split(' - ')[0])
        .sort()
        .join(', ');
    }
    return null;
  }

  @Expose()
  role() {
    if (this.roles.length) {
      return this.roles.map(r => r.description).join(', ');
    }

    return 'Não definido';
  }

  @OneToOne(() => Client, client => client.user, {
    cascade: true,
    eager: true,
  })
  client: Client;

  @OneToOne(() => UserProfile, profile => profile.user, {
    cascade: true,
    eager: true,
  })
  profile: UserProfile;

  @OneToMany(() => Welder, welders => welders.user, { eager: true })
  welders: Welder[];
}

export { User };
