import {
  getRepository,
  ILike,
  IsNull,
  Not,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';

import { ICreateUserDTO } from '@modules/users/dtos/ICreateUserDTO';
import { IUsersRepository } from '@modules/users/repositories/IUsersRepository';
import { ISearchParamsDTO } from '@modules/users/dtos/ISearchParamsDTO';
import { ISearchResponseDTO } from '@modules/users/dtos/ISearchResponseDTO';
import { User } from '../entities/User';

class UsersRepository implements IUsersRepository {
  private repository: Repository<User>;

  constructor() {
    this.repository = getRepository(User);
  }

  async create(data: ICreateUserDTO): Promise<User> {
    const user = this.repository.create(data);

    await this.repository.save(user);

    return user;
  }

  async findById(id: string, relations?: string[]): Promise<User> {
    return this.repository.findOne(id, {
      relations,
    });
  }

  async findByEmail(email: string): Promise<User> {
    return this.repository.findOne({ where: { email } });
  }

  async findByAvatar(avatar: string): Promise<User> {
    return this.repository.findOne({ where: { avatar } });
  }

  async save(user: User): Promise<User> {
    return this.repository.save(user);
  }

  async list({
    filter,
    query,
    page,
    per_page,
  }: ISearchParamsDTO): Promise<ISearchResponseDTO> {
    let filterParams = {};
    let queryParams = {};

    switch (filter) {
      case 'clients':
        filterParams = {
          client: {
            id: Not(IsNull()),
          },
        };
        break;
      case 'users':
        filterParams = { profile: { id: Not(IsNull()) } };
        break;
      default:
    }

    if (query) {
      switch (filter) {
        case 'clients':
          queryParams = [
            { name: ILike(`%${query}%`) },
            { client: { cnpj: ILike(`%${query}%`) } },
          ];
          break;
        case 'users':
          queryParams = [
            { name: ILike(`%${query}%`) },
            { profile: { company: ILike(`%${query}%`) } },
          ];
          break;
        default:
          queryParams = [{ name: ILike(`%${query}%`) }];
      }
    }

    const [users, total] = await this.repository.findAndCount({
      relations: ['client', 'profile'],
      where: (qb: SelectQueryBuilder<User>) => {
        qb.where(queryParams).andWhere(filterParams);
      },
      skip: page - 1,
      take: per_page,
    });

    return { users, total };
  }

  async delete(user_id: string): Promise<void> {
    await this.repository.delete(user_id);
  }
}

export { UsersRepository };
