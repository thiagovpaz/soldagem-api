import { Router } from 'express';

import { SendForgotPasswordMailController } from '@modules/users/useCases/sendForgotPasswordMail/SendForgotPasswordMailController';
import { ResetPasswordUserController } from '@modules/users/useCases/resetPasswordUser/ResetPasswordUserController';

const passwordsRouter = Router();

const sendForgotPasswordMailController = new SendForgotPasswordMailController();
const resetPasswordController = new ResetPasswordUserController();

passwordsRouter.post('/forgot', sendForgotPasswordMailController.handle);
passwordsRouter.post('/reset', resetPasswordController.handle);

export { passwordsRouter };
