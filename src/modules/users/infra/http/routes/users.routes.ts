import { Router } from 'express';
import multer from 'multer';

import uploadConfig from '@config/upload';

import { CreateUserController } from '@modules/users/useCases/createUser/CreateUserController';

import { can, is } from '@modules/acl/infra/http/middlewares/permissions';
import { UploadAvatarController } from '@modules/users/useCases/uploadAvatar/UploadAvatarController';
import { GetUserController } from '@modules/users/useCases/getUser/GetUserController';
import { ListUsersController } from '@modules/users/useCases/listUsers/ListUsersController';
import { UpdateUserController } from '@modules/users/useCases/updateUser/UpdateUserController';
import { DeleteUserController } from '@modules/users/useCases/deleteUser/DeleteUserController';
import { DeleteAvatarController } from '@modules/users/useCases/deleteAvatar/DeleteAvatarController';
import ensureAuthenticated from '../middlewares/ensureAuthenticated';

const usersRouter = Router();
const upload = multer(uploadConfig.multer);

const createUserController = new CreateUserController();
const uploadAvatarController = new UploadAvatarController();
const getUserController = new GetUserController();
const listUsersController = new ListUsersController();
const updateUserController = new UpdateUserController();
const deleteUserController = new DeleteUserController();
const deleteAvatarController = new DeleteAvatarController();

usersRouter.use(ensureAuthenticated);
usersRouter.post(
  '/avatar',
  upload.single('avatar'),
  uploadAvatarController.handle,
);
usersRouter.delete('/avatar', deleteAvatarController.handle);

usersRouter.use(is(['ADMIN']), can(['*']));
usersRouter.post('/', createUserController.handle);
usersRouter.get('/:user_id', getUserController.handle);
usersRouter.get('/', listUsersController.handle);
usersRouter.put('/:user_id', updateUserController.handle);
usersRouter.delete('/', deleteUserController.handle);

export { usersRouter };
