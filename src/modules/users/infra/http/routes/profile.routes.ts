import { Router } from 'express';

import { UpdateProfileController } from '@modules/users/useCases/updateProfile/UpdateProfileController';
import { ProfileUserController } from '@modules/users/useCases/profileUser/ProfileUserController';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

const profileRouter = Router();

const updateProfileController = new UpdateProfileController();
const profileUserController = new ProfileUserController();

profileRouter.put('/', ensureAuthenticated, updateProfileController.handle);
profileRouter.get('/', ensureAuthenticated, profileUserController.handle);

export { profileRouter };
