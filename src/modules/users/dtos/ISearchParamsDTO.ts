interface ISearchParamsDTO {
  filter?: string;
  query?: string | null;
  page?: number;
  per_page?: number;
}
export { ISearchParamsDTO };
