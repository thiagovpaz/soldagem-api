import { ICreateUserClientDTO } from './ICreateClientDTO';
import { ICreateProfileDTO } from './ICreateProfileDTO';

interface ICreateUserDTO {
  name: string;
  email: string;
  password: string;
  avatar?: string;
  client?: ICreateUserClientDTO;
  profile?: ICreateProfileDTO;
}

export { ICreateUserDTO };
