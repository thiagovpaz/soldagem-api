import { Permission } from '@modules/acl/infra/typeorm/entities/Permission';
import { Role } from '@modules/acl/infra/typeorm/entities/Role';
import { Client } from '../infra/typeorm/entities/Client';
import { UserProfile } from '../infra/typeorm/entities/UserProfile';

interface IUserResponseDTO {
  id: string;
  name: string;
  email: string;
  avatar?: string;
  permissions: Permission[];
  roles: Role[];
  client?: Client;
  profile?: UserProfile;
  status: boolean;
}

export { IUserResponseDTO };
