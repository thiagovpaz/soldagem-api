interface ICreateProfileDTO {
  company?: string;
  register?: string;
  cpf?: string;
  birthdate?: Date;
  phone?: string;
  qualification?: string;
  position?: string;
  certifying_entity?: string;
  certification_validity?: Date;
}

export { ICreateProfileDTO };
