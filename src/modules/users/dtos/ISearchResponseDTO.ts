import { User } from '@modules/users/infra/typeorm/entities/User';

interface ISearchResponseDTO {
  users: User[];
  total: number;
}

export { ISearchResponseDTO };
