module.exports = {
  apps : [{
    name: "soldagem-api",
    script: "./dist/shared/infra/http/server.js",
    env_production: {
      NODE_ENV: "production",
    }
  }]
}
